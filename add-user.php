<?php
include "application-top.php";

if (!isset($_SESSION["email"])) {
  header("location:index.php");
  exit();
}

$url_sql = "select * from mtc_usertypes where status = 1";
$url_res = mysqli_query($con, $url_sql);
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php echo $gbl_row["org_name"]; ?></title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/feather/feather.css">
  <link rel="stylesheet" href="vendors/ti-icons/css/themify-icons.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <link rel="stylesheet" href="vendors/select2/select2.min.css">
  <link rel="stylesheet" href="vendors/select2-bootstrap-theme/select2-bootstrap.min.css">
  <link rel="stylesheet" href="js/cute-alert-master/alert-style.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/vertical-layout-light/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php include "includes/header.php"; ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
      <?php include "includes/right-sidebar.php"; ?>
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
      <?php include "includes/sidebar-menu.php"; ?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <?php create_breadcrumb(); ?>
          <div class="row">
            <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Add New User</h4>
                  <p class="card-description">
                    Personal info
                  </p>
                  <form class="forms-sample" id="addUser" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                      <label for="username">Name</label>
                      <input type="text" name="username" class="form-control" id="username" placeholder="Name">
                    </div>
                    <div class="form-group">
                      <label for="email">Email address</label>
                      <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                    </div>

                    <div id="loader">
                      <img id="loading-image" src="images/loader.gif" style="display:none;" />
                    </div>

                    <div class="form-group">
                      <label for="password">Password</label>
                      <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                    </div>

                    <div class="form-group">
                      <label for="usertype">User Role</label>
                      <select class="form-control" name="usertype" id="usertype">
                        <option value="" selected disabled>Please select</option>
                        <?php if ($url_res) {
                          while ($url_row = mysqli_fetch_array($url_res)) { ?>
                            <option value="<?php echo $url_row["usertype_id"]; ?>"><?php echo ucfirst($url_row["usertype_name"]); ?></option>
                        <?php }
                        } ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>File upload</label>
                      <input type="file" name="proflle_image" id="proflle_image" class="file-upload-default">
                      <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                        <span class="input-group-append">
                          <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                        </span>
                      </div>
                    </div>

                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button class="btn btn-light" type="reset">Cancel</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <?php include "includes/footer.php"; ?>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <?php include "includes/common-js.php"; ?>
  <!-- End custom js for this page-->
  <script>
    $(document).ready(function() {

      var isValid = false;

      $("#addUser").on("submit", function(e) {
        e.preventDefault();

        var username = $("#username").val();
        var email = $("#email").val();
        var password = $("#password").val();
        var usertype = $("#usertype").val();
        var userData = new FormData();
        userData.append("username", username);
        userData.append("email", email);
        userData.append("password", password);
        userData.append("usertype", usertype);
        userData.append("proflle_image", $('input[type=file]')[0].files[0]);

        isvalid = checkFormStatus("addUser");

        if (isvalid) {
          $.ajax({
            type: "POST",
            url: "ajax/add-new-user.php",
            data: userData,
            dataType: "html",
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
              $("#loading-image").show();
            },
            success: function(response) {
              if (response == 0) {
                addUser.reset();
                cuteToast({
                  type: "success",
                  message: "User added successfully...",
                  timer: 3000
                });
                setTimeout(function() {
                  location.href = "view-users.php"
                }, 3000);
                $("#loading-image").hide();
              } else if (response == 2) {
                cuteToast({
                  type: "warning",
                  message: "Email id already exist...",
                  timer: 3000
                });
                $("#loading-image").hide();
              } else if (response == 3) {
                addUser.reset();
                cuteToast({
                  type: "warning",
                  message: "Invalid file type, please choose another file.",
                  timer: 3000
                });
                $("#loading-image").hide();
              } else {
                addUser.reset();
                cuteToast({
                  type: "error",
                  message: "Something went wrong...",
                  timer: 3000
                });
                $("#loading-image").hide();
              }
            }
          })
        }
      });
    });
  </script>
</body>

</html>