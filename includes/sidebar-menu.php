<?php
$prvsql = "select * from mtc_user_previleges where usertype_id =" . $_SESSION["usertype"];
$prvres = mysqli_query($con, $prvsql);
$prvrow = mysqli_fetch_array($prvres);
?>
<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item">
      <a class="nav-link" href="dashboard.php">
        <i class="icon-grid menu-icon"></i>
        <span class="menu-title">Dashboard</span>
      </a>
    </li>
    <?php
    if ($prvrow["user_module_settings"] == 1) {
    ?>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#users" aria-expanded="false" aria-controls="users">
          <i class="icon-head menu-icon"></i>
          <span class="menu-title">User Manager</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="users">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="add-user.php">Add New User</a></li>
            <li class="nav-item"> <a class="nav-link" href="view-users.php">View Users</a>
            </li>
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#customers" aria-expanded="false" aria-controls="customers">
          <i class="icon-contract menu-icon"></i>
          <span class="menu-title">Customer Manager</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="customers">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="add-customer.php">Add New Customer</a></li>
            <li class="nav-item"> <a class="nav-link" href="view-customers.php">View Customers</a>
            </li>
          </ul>
        </div>
      </li>
    <?php }
    if ($prvrow["product_module_settings"] == 1) {
    ?>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#prods" aria-expanded="false" aria-controls="prods">
          <i class="icon-folder menu-icon"></i>
          <span class="menu-title">Product Manager</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="prods">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="add-product.php">Add New Product</a></li>
            <li class="nav-item"> <a class="nav-link" href="view-products.php">View Products</a>
            </li>
          </ul>
        </div>
      </li>
    <?php }
    if ($prvrow["invoice_module_settings"] == 1) {
    ?>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#orders" aria-expanded="false" aria-controls="orders">
          <i class="icon-paper-stack menu-icon"></i>
          <span class="menu-title">Order Manager</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="orders">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="create-order.php">Add New Order</a></li>
            <li class="nav-item"> <a class="nav-link" href="view-orders.php">View Orders</a>
            </li>
          </ul>
        </div>
      </li>
    <?php }
    if ($prvrow["purchase_module_settings"] == 1) {
    ?>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#purs" aria-expanded="false" aria-controls="purs">
          <i class="icon-paper menu-icon"></i>
          <span class="menu-title">Purchase Manager</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="purs">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="create-purchase-order.php">Add New Order</a></li>
            <li class="nav-item"> <a class="nav-link" href="view-purchase-orders.php">View Orders</a>
            </li>
          </ul>
        </div>
      </li>
    <?php }
    if ($prvrow["purchase_module_settings"] == 1 || $prvrow["invoice_module_settings"] == 1) {
    ?>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#reports" aria-expanded="false" aria-controls="reports">
          <i class="icon-layout menu-icon"></i>
          <span class="menu-title">Report Manager</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="reports">
          <ul class="nav flex-column sub-menu">
            <?php if ($prvrow["invoice_module_settings"] == 1) { ?>
              <li class="nav-item"><a class="nav-link" href="sales-report.php">Sales Report</a></li>
            <?php } ?>
            <?php if ($prvrow["purchase_module_settings"] == 1) { ?>
              <li class="nav-item"><a class="nav-link" href="purchase-report.php">Purchase Report</a>
              <?php } ?>
              </li>
          </ul>
        </div>
      </li>
    <?php
    }
    ?>
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
        <i class="icon-layout menu-icon"></i>
        <span class="menu-title">UI Elements</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="ui-basic">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="pages/ui-features/buttons.html">Buttons</a></li>
          <li class="nav-item"> <a class="nav-link" href="pages/ui-features/dropdowns.html">Dropdowns</a>
          </li>
          <li class="nav-item"> <a class="nav-link" href="pages/ui-features/typography.html">Typography</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#form-elements" aria-expanded="false" aria-controls="form-elements">
        <i class="icon-columns menu-icon"></i>
        <span class="menu-title">Form elements</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="form-elements">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"><a class="nav-link" href="pages/forms/basic_elements.html">Basic Elements</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#tables" aria-expanded="false" aria-controls="tables">
        <i class="icon-grid-2 menu-icon"></i>
        <span class="menu-title">Tables</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="tables">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="pages/tables/basic-table.html">Basic table</a>
          </li>
        </ul>
      </div>
    </li>
  </ul>
</nav>