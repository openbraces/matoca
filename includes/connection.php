<?php
include_once "config.php";

// Connecting to database

$con = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

// Check connection

if (mysqli_connect_errno()) {
    echo "Failed to connect to Database Server: " . mysqli_connect_error();
}
