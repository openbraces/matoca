<?php

function getInvoiceCount()
{
    global $con;
    $sql = "select count(*) from mtc_invoices";
    $res = mysqli_query($con, $sql);
    $row = mysqli_fetch_array($res);
    $total = $row[0];
    return $total;
}

function getPurchaseInvoiceCount()
{
    global $con;
    $sql = "select count(*) from mtc_orders";
    $res = mysqli_query($con, $sql);
    $row = mysqli_fetch_array($res);
    $total = $row[0];
    return $total;
}

function getUsertypeCount()
{
    global $con;
    $sql = "select count(*) as total from mtc_usertypes";
    $res = mysqli_query($con, $sql);
    $row = mysqli_fetch_object($res);
    $total = $row->total;
    return $total;
}

function getUserCount()
{
    global $con;
    $sql = "select count(*) as total from mtc_users";
    $res = mysqli_query($con, $sql);
    $row = mysqli_fetch_object($res);
    $total = $row->total;
    return $total;
}

function getProductCount()
{
    global $con;
    $sql = "select count(*) as total from mtc_products";
    $res = mysqli_query($con, $sql);
    $row = mysqli_fetch_object($res);
    $total = $row->total;
    return $total;
}

function create_breadcrumb()
{
    $req_url = $_SERVER['REQUEST_URI'];
    $md_url = str_replace('/mtc/', '', $req_url);
    $page_name = substr($md_url, 0, strpos($md_url, '.'));
    $page_title = str_replace("-", " ", $page_name);

    if ($page_name == "Dashboard" || $page_name == "dashboard") {
        echo ucfirst($page_name);
    } else {
        echo '<ul class="breadcrumb" style="background-color: #ffffff; border-radius: 0.5rem">
                   <li class="breadcrumb-item"><a href="dashboard.php">Home</a></li>
                   <li class="breadcrumb-item active">' . ucwords($page_title) . '</li>
               </ul>';
    }

}

/* Added in last 30 days */

function getUserCountbyDays()
{
    global $con;
    $sql = "select count(*) as total from mtc_users where created_on between NOW() - INTERVAL 30 DAY AND NOW()";
    $res = mysqli_query($con, $sql);
    $row = mysqli_fetch_object($res);
    $total = $row->total;
    return $total;
}

/* Added in last 30 days */

function getProductCountbyDays()
{
    global $con;
    $sql = "select count(*) as total from mtc_products where added_on between NOW() - INTERVAL 30 DAY AND NOW()";
    $res = mysqli_query($con, $sql);
    $row = mysqli_fetch_object($res);
    $total = $row->total;
    return $total;
}

/* Purchase in last 30 days */

function totalMonthlyPurchase()
{
    global $con;
    $sql = "select sum(invoice_total) as totalAmount from mtc_orders where created_on between NOW() - INTERVAL 30 DAY AND NOW()";
    $res = mysqli_query($con, $sql);
    $row = mysqli_fetch_object($res);
    $total = $row->totalAmount;
    return $total;

}

/* Sales in last 30 days */

function totalMonthlySales()
{
    global $con;
    $sql = "select sum(invoice_total) as totalAmount from mtc_invoices where created_on between NOW() - INTERVAL 30 DAY AND NOW()";
    $res = mysqli_query($con, $sql);
    $row = mysqli_fetch_object($res);
    $total = $row->totalAmount;
    return $total;

}


function _etize($data)
{
    global $con;
    $data = mysqli_real_escape_string($con, $data);
    $data = htmlentities($data);
    $data = trim($data);
    $data = strip_tags($data);
    $data = htmlspecialchars($data);
    return $data;
}

function getIP() {  
    //whether ip is from the share internet  
     if(!empty($_SERVER['HTTP_CLIENT_IP'])) {  
          $ipAddr = $_SERVER['HTTP_CLIENT_IP'];  
        }  
    //whether ip is from the proxy  
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {  
          $ipAddr = $_SERVER['HTTP_X_FORWARDED_FOR'];  
     }  
    //whether ip is from the remote address  
    else{  
          $ipAddr = $_SERVER['REMOTE_ADDR'];  
     }  
     return $ipAddr;  
}  


