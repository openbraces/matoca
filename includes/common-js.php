  <!-- plugins:js -->
  <script src="vendors/js/vendor.bundle.base.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
  <!-- endinject -->
  <!-- jQuery UI -->
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

  <!-- Jquery Validator lib -->
  <script src="js/jquery.validate.min.js"></script>
  <link rel="stylesheet" href="css/validator.css">
  <script src="js/validator-utility.js"></script>

  <!-- Plugin js for this page -->
  <script src="vendors/chart.js/Chart.min.js"></script>
  <script src="vendors/datatables.net/jquery.dataTables.js"></script>
  <script src="vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
  <script src="js/dataTables.select.min.js"></script>
  <script src="vendors/typeahead.js/typeahead.bundle.min.js"></script>
  <script src="vendors/select2/select2.min.js"></script>
  <script src="js/encryption.js"></script>
  <script src="js/crypto-js.min.js"></script>

  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="js/off-canvas.js"></script>
  <script src="js/hoverable-collapse.js"></script>
  <script src="js/template.js"></script>
  <script src="js/settings.js"></script>
  <script src="js/todolist.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page
  <script src="js/dashboard.js"></script>-->
  <script src="js/Chart.roundedBarCharts.js"></script>
  <!-- End custom js for this page-->

  <!-- Custom js for this page-->
  <script src="js/file-upload.js"></script>
  <script src="js/typeahead.js"></script>
  <script src="js/select2.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <!-- End custom js for this page-->

<!-- Cute alert  -->
<script src="js/cute-alert-master/cute-alert.js"></script>

<script src="js/paging.js"></script>


<script>
jQuery(document).ready(function($) {

$("#error").hide();

$("#loginForm").on('submit', function(e) {
e.preventDefault();

var username = $("#username").val();
var password = $("#password").val();
let encryption = new Encryption();
var cipherValue = $('#cryptoKey').val();
var rememberMe;

if ($('#rememberMe').is(':checked') || $('#rememberMe').prop('checked')) {
    // do something if checked
    rememberMe = $("#rememberMe").val();
} else {
    // do something if unchecked
    rememberMe = 0;
}

var dataStr = $('#loginForm').serializeArray().reduce(function(obj, item) {
    if (item.name != "cryptoKey" && item.name != "rememberMe") {
        obj[item.name] = encryption.encrypt(item.value, cipherValue);
    } else {
        obj[item.name] = item.value;
    }
    return obj;
}, {});

if (username == "" || password == "") {
    alert(dataStr);
} else {
    $.ajax({
        type: "POST",
        url: "verify-login.php",
        data: dataStr,
        cache: false,
        success: function(response) {
             if (response == 1) {
                 cuteToast({
                    type: "success",
                    message: "Login was successful...",
                    timer: 2000
                    });
                setTimeout(function() {
                       window.location.href  = "dashboard.php"
                    }, 3000);
                } else {
             $("#error").show().fadeOut(3000).html(response);
             $("form").trigger("reset");
             setTimeout(function() {
                       window.location.href  = "index.php"
                }, 3000);
            }
            $("#failurelogs").fadeOut(3000);
        }
    });
}
});
});

</script>
