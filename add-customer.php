<?php
include "application-top.php";

if (!isset($_SESSION["email"])) {
    header("location:index.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php echo $gbl_row["org_name"]; ?></title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/feather/feather.css">
  <link rel="stylesheet" href="vendors/ti-icons/css/themify-icons.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <link rel="stylesheet" href="vendors/select2/select2.min.css">
  <link rel="stylesheet" href="vendors/select2-bootstrap-theme/select2-bootstrap.min.css">
  <link rel="stylesheet" href="js/cute-alert-master/alert-style.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/vertical-layout-light/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
  <?php include "includes/header.php";?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
       <?php include "includes/right-sidebar.php";?>
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
    <?php include "includes/sidebar-menu.php";?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <?php create_breadcrumb();?>
          <div class="row">
            <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Add New Customer</h4>
                  <p class="card-description">
                    Customer info
                  </p>
                  <form class="forms-sample" id="addCustomer" method="post">
                    <div class="form-group">
                      <label for="customer_name">Customer Name</label>
                      <input type="text" name="customer_name"  class="form-control" id="customer_name" placeholder="Customer Name">
                    </div>

                    <div class="form-group">
                      <label for="customer_email">Customer Email</label>
                      <input type="email" name="customer_email" id="customer_email" class="form-control" placeholder="Customer Email">
                    </div>

                    <div class="form-group">
                      <label for="contact_number">Contact No.</label>
                      <input type="text" name="contact_number" id="contact_number" maxlength="14" class="form-control" placeholder="Contact Number">
                    </div>

                     <div id="loader">
                        <img id="loading-image" src="images/loader.gif" style="display:none;"/>
                    </div>

                    <div class="form-group">
                      <label for="customer_address">Address</label>
                      <input type="text" name="customer_address" class="form-control" id="customer_address" placeholder="Customer Address">
                    </div>
                    
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button class="btn btn-light" type="reset">Cancel</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <?php include "includes/footer.php";?>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <?php include "includes/common-js.php";?>
  <!-- End custom js for this page-->
  <script>

   $(document).ready(function() {
     
      var isValid = false;
       
       $("#addCustomer").on("submit", function(e) {
           e.preventDefault();

           var customer_name = $("#customer_name").val();
           var customer_email = $("#customer_email").val();
           var contact_number = $("#contact_number").val();
           var customer_address = $("#customer_address").val();
           var customerData = new FormData();
           customerData.append("customer_name", customer_name);
           customerData.append("customer_email", customer_email);
           customerData.append("contact_number", contact_number);
           customerData.append("customer_address", customer_address);
         
           isvalid  = checkFormStatus("addCustomer");

           if(isvalid)
           {            
           $.ajax({
                type : "POST",
                url    : "ajax/add-new-customer.php",
                data : customerData,
                dataType: "html",
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                  $("#loading-image").show();
               },
               success: function(response) {
                  if (response == 0) {
                    addCustomer.reset();
                    cuteToast({
                    type: "success",
                    message: "Customer added successfully...",
                    timer: 3000
                    });
                    setTimeout(function() {
                        location.href = "view-customers.php"
                    }, 3000);
                    $("#loading-image").hide();
                  } else {
                    addCustomer.reset();
                    cuteToast({
                    type: "error",
                    message: "Something went wrong...",
                    timer: 3000
                    });
                    $("#loading-image").hide();
                }
               }
           })
          }
       });
   });

  </script>
</body>
</html>
