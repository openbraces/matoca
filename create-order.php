<?php
include "application-top.php";
include 'order-invoice.php';

if (!isset($_SESSION["email"])) {
   header("location:index.php");
   exit();
}

$cms_sql = "select * from mtc_customers where status = 1";
$cm_res = mysqli_query($con, $cms_sql);

if (!empty($_POST)) {
   try {
      $data = saveInvoice($_POST);

      if (isset($data['success']) && $data['success']) {
         $_SESSION['success'] = 'Invoice Saved Successfully!';
         header('location: generate-invoice.php?invoice_id=' . $data["order_id"]);
         exit;
      } else {
         $_SESSION['success'] = 'Invoice Save failed, try again.';
      }
   } catch (Exception $e) {
      $_SESSION['error'] = $e->getMessage();
   }
}

/*  Invoice Number Generator   */
$inv = getInvoiceCount() + 1;
$inv = "MTC-" . str_pad($inv, 5, "0", STR_PAD_LEFT);
/*  Invoice Number Generator   */

?>
<!DOCTYPE html>
<html>

<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <title><?php echo $gbl_row["org_name"]; ?></title>
   <link href="css/jquery-ui.min.css" rel="stylesheet">
   <link href="css/datepicker.css" rel="stylesheet">
   <link href="css/font-awesome.min.css" rel="stylesheet">
   <link href="css/invoice-style.css" rel="stylesheet">
   <!-- plugins:css -->
   <link rel="stylesheet" href="vendors/feather/feather.css">
   <link rel="stylesheet" href="vendors/ti-icons/css/themify-icons.css">
   <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
   <link rel="stylesheet" href="vendors/select2/select2.min.css">
   <!-- endinject -->
   <!-- Plugin css for this page -->
   <link rel="stylesheet" href="js/cute-alert-master/alert-style.css">
   <!-- End plugin css for this page -->
   <!-- inject:css -->
   <link rel="stylesheet" href="css/vertical-layout-light/style.css">
   <!-- endinject -->
   <link rel="shortcut icon" href="images/favicon.png" />
</head>

<body>
   <div class="container-scroller">
      <?php include "includes/header.php"; ?>
      <!-- Insert your HTML here -->
      <div class="container-fluid page-body-wrapper">
         <?php include "includes/right-sidebar.php"; ?>
         <!-- partial -->
         <!-- partial:partials/_sidebar.html -->
         <?php include "includes/sidebar-menu.php"; ?>
         <!-- partial
               </div>-->
         <div class="main-panel">
            <div class="content-wrapper">
               <div class="row">
                  <div class="col-lg-12 grid-margin stretch-card">
                     <div class="card">
                        <div class="card-body">
                           <form class="form-horizontal invoice-form" action="" id="invoice-form" method="post" role="form" novalidate>
                              <div class='row no-margin'>
                                 <div class='col-xs-12 col-sm-4 col-md-4 col-lg-4'>
                                    <div class="logo">
                                       <img src="upload_images/<?php echo $gbl_row["company_logo"]; ?>" alt="Company Logo">
                                    </div>
                                    <h4> <?php echo $gbl_row["org_name"]; ?></h4>
                                    <p>
                                       <?php echo $gbl_row["org_info"]; ?>
                                    </p>
                                 </div>
                                 <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4"></div>
                                 <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group">
                                       <input type="text" class="form-control  form-control-sm" readonly name="invoiceNo" id="invoiceNo" value="<?php echo $inv; ?>" placeholder="Invoice No">
                                    </div>
                                    <div class="form-group">
                                       <input type="text" class="form-control  form-control-sm" readonly name="invoiceDate" value="<?php echo date('d-m-Y'); ?>" placeholder="Invoice Date">
                                    </div>
                                 </div>
                              </div>
                              <hr>
                              <div class="row no-margin">
                                 <div class="col-md-12">
                                    <h4 class="text-white text-center text-capitalize bg-primary pl-1 pt-1 pb-1 mb-4">Customer Details</h4>
                                 </div>
                                 <div class='col'>
                                    <div class="form-group">
                                       <select class="form-control form-control-sm customerList" required name="customer" id="customer">
                                          <option disabled selected>Select Customer</option>
                                          <?php
                                          if ($cm_res) {
                                             while ($cm_row = mysqli_fetch_array($cm_res)) {
                                          ?>
                                                <option value="<?php echo $cm_row["customer_id"]; ?>"><?php echo $cm_row["customer_name"]; ?></option>
                                          <?php
                                             }
                                          }
                                          ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class='col'>
                                    <div class="form-group">
                                       <input type="text" class="form-control form-control-sm" readonly name="customerContact" id="customerContact" placeholder="Customer Contact" value="">
                                    </div>
                                 </div>
                                 <div class='col-md-6'>
                                    <div class="form-group">
                                       <input type="text" class="form-control form-control-sm" readonly name="customerAddress" id="customerAddress" placeholder="Company Address" value="">
                                    </div>
                                    <input type="hidden" value="<?php echo $_SESSION['client_id']; ?>" name="client_id">
                                    <input type="hidden" value="" name="id">
                                    <input type="hidden" value="1" name="uuid">
                                 </div>
                              </div>
                              <hr>
                              <div class=''>
                                 <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                                    <table class="table table-bordered table-hover" id="invoiceTable">
                                       <thead>
                                          <tr>
                                             <th width="2%"><input id="check_all" class="formcontrol" type="checkbox" /></th>
                                             <th width="15%">Product SKU</th>
                                             <th width="38%">Product Name</th>
                                             <th width="15%">Price</th>
                                             <th width="15%">Quantity</th>
                                             <th width="15%">Total</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <tr>
                                             <td><input class="case" type="checkbox" /></td>
                                             <td><input type="text" required data-type="product_sku_code" name="product_sku_code[]" id="itemNo_1" class="form-control form-control-sm autocomplete_txt" autocomplete="off"></td>
                                             <input type="hidden" class="productClass" data-type="product_id" name="product_id[]" id="prodID_1">
                                             <td><input type="text" required data-type="product_name" name="product_name[]" id="itemName_1" class="form-control form-control-sm autocomplete_txt" autocomplete="off"></td>
                                             <td><input type="number" name="price[]" id="price_1" class="form-control form-control-sm changesNo" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>
                                             <td><input type="number" name="quantity[]" id="quantity_1" class="form-control form-control-sm changesNo" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>
                                             <td><input type="number" name="total[]" id="total_1" class="form-control form-control-sm totalLinePrice" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                              <div class='mt-3'>
                                 <div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>
                                    <button id="delete" class="btn btn-danger delete" type="button">- Delete</button>
                                    <button id="addmore" class="btn btn-success addmore" type="button">+ Add More</button>
                                 </div>
                                 <div class="row no-margin mt-3">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label>Subtotal: &nbsp;</label>
                                          <div class="input-group">
                                             <div class="input-group-prepend">
                                                <span class="input-group-text bg-info text-white">₹</span>
                                             </div>
                                             <input type="number" class="form-control form-control-sm" name="invoice_subtotal" id="subTotal" placeholder="Subtotal" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label>Total: &nbsp;</label>
                                          <div class="input-group">
                                             <div class="input-group-prepend">
                                                <span class="input-group-text bg-info text-white">₹</span>
                                             </div>
                                             <input type="number" class="form-control form-control-sm" name="invoice_total" id="totalAftertax" placeholder="Total" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row no-margin">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label>Tax: &nbsp;</label>
                                          <div class="input-group">
                                             <div class="input-group-prepend">
                                                <span class="input-group-text bg-info text-white">₹</span>
                                             </div>
                                             <input type="number" class="form-control form-control-sm" name="tax_percent" id="tax" placeholder="Tax" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label>Amount Paid: &nbsp;</label>
                                          <div class="input-group">
                                             <div class="input-group-prepend">
                                                <span class="input-group-text bg-info text-white">₹</span>
                                             </div>
                                             <input type="number" required class="form-control form-control-sm" name="amount_paid" id="amountPaid" placeholder="Amount Paid" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row no-margin">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label>Tax Amount: &nbsp;</label>
                                          <div class="input-group">
                                             <input type="number" class="form-control form-control-sm" name="tax" id="taxAmount" placeholder="Tax" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;">
                                             <div class="input-group-addon">%</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label>Amount Due: &nbsp;</label>
                                          <div class="input-group">
                                             <div class="input-group">
                                                <div class="input-group-prepend">
                                                   <span class="input-group-text bg-info text-white">₹</span>
                                                </div>
                                                <input type="number" class="form-control form-control-sm amountDue" name="amount_due" id="amountDue" placeholder="Amount Due" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class='row'>
                                    <div class="col-xs-12 col-sm-12">
                                       <div class="text-center mb-5">
                                          <button data-loading-text="Saving Invoice..." type="submit" name="submit" class="btn btn-info submit_btn invoice-save-bottom">
                                             <i class="ti-file btn-icon-prepend"></i>
                                             Save Invoice </button>
                                       </div>
                                    </div>
                                 </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- /container -->
         <?php include "includes/footer.php"; ?>
      </div>
      <!-- main-panel ends -->
   </div>
   <!-- page-body-wrapper ends -->
   </div>
   <!-- container-scroller -->
   <?php include "includes/common-js.php"; ?>
   <script src="js/auto.js"></script>
   <script>
      jQuery(document).ready(function($) {
         $('.customerList').select2().on("change", function(e) {
            let cid = Number($("#customer").val());
            console.log(cid);
            $.ajax({
               url: 'ajax/get-customer.php?cid=' + cid,
               type: 'GET',
               success: function(data) {
                  $("#customerContact").val(JSON.parse(data)[0].contact_number).attr('disabled', true);
                  $("#customerAddress").val(JSON.parse(data)[0].customer_address).attr('disabled', true);
               },
               error: function(e) {
                  alert("Error Occured " + e);
               }
            });
         });

         $('form').on('submit', function(e) {
            const prdArr = [];
            $('input.productClass').each(function() {
               prdArr.push($(this).val()); 
            });

            const isDuplicate = prdArr.some((ele, i, arr) => arr.indexOf(ele) !== i);

            if(isDuplicate) {
               cuteAlert({
                  type: "error",
                  title: "Duplicate Data",
                  message: "Please remove the duplicate products",
                  buttonText: "Okay"
               });
               return false;
            } else {
               $('form')[0].submit();
            }   
            return true;
         });
      });
   </script>
</body>

</html>