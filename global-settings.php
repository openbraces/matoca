<?php
include "application-top.php";

if (!isset($_SESSION["email"])) {
    header("location:index.php");
    exit();
}

$sql = "select * from mtc_global_settings where setting_id=1";
$res = mysqli_query($con, $sql);
$row = mysqli_fetch_array($res);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo $gbl_row["org_name"]; ?></title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="vendors/feather/feather.css">
    <link rel="stylesheet" href="vendors/ti-icons/css/themify-icons.css">
    <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="vendors/select2/select2.min.css">
    <link rel="stylesheet" href="vendors/select2-bootstrap-theme/select2-bootstrap.min.css">
    <link rel="stylesheet" href="js/cute-alert-master/alert-style.css">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="css/vertical-layout-light/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="images/favicon.png" />
    <style>
    #overlay {
  position: fixed; /* Sit on top of the page content */
  display: none; /* Hidden by default */
  width: 100%; /* Full width (cover the whole page) */
  height: 100%; /* Full height (cover the whole page) */
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0,0,0,0.5); /* Black background with opacity */
  z-index: 2; /* Specify a stack order in case you're using a different order for other elements */
  cursor: pointer; /* Add a pointer on hover */
}
</style>
</head>

<body>
    <div class="container-scroller">
        <!-- partial:partials/_navbar.html -->
        <?php include "includes/header.php";?>
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <!-- partial:partials/_settings-panel.html -->
            <?php include "includes/right-sidebar.php";?>
            <!-- partial -->
            <!-- partial:partials/_sidebar.html -->
            <?php include "includes/sidebar-menu.php";?>
            <!-- partial -->
            <div class="main-panel">
                <div class="content-wrapper">
                     <?php create_breadcrumb();?>
                    <div class="row">
                        <div class="col-12 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Global Configurations</h4>
                                    <p class="card-description">
                                        App info
                                    </p>
                                    <form class="forms-sample" id="gbSett" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label for="org_name">Company Name</label>
                                            <input type="text" required name="org_name" required class="form-control"
                                                id="org_name" value="<?php echo $row["org_name"]; ?>">
                                        </div>

                                        <div class="form-group">
                                            <label for="org_info">Company Info</label>
                                            <input type="text" name="org_info" id="org_info" class="form-control"
                                                value="<?php echo $row["org_info"]; ?>">
                                        </div>

                                        <div class="form-group">
                                            <label for="admin_email">Admin Email</label>
                                            <input type="email" name="admin_email" id="admin_email" class="form-control"
                                                value="<?php echo $row["admin_email"]; ?>">
                                        </div>

                                        <div class="form-group">
                                            <label for="site_url">Site Url</label>
                                            <input type="text" name="site_url" id="site_url" class="form-control"
                                                value="<?php echo $row["site_url"]; ?>">
                                        </div>

                                        <div id="loader">
                                            <img id="loading-image" src="images/loader.gif" style="display:none;" />
                                        </div>
                                        <div id="overlay"></div>

                                        <div class="form-group">
                                            <label for="contact_no">Contact No.</label>
                                            <input type="text" name="contact_no" class="form-control" id="contact_no"
                                                value="<?php echo $row["contact_no"]; ?>">
                                        </div>

                                      <div class="form-group">
                                       <label>Existing Image</label>
                                      <div class="input-group col-xs-12">
                                          <img src="upload_images/<?php echo $row["company_logo"]; ?>" class="rounded float-left" alt="<?php echo $row["org_name"]; ?>">
                                       </div>
                                    </div>

                                        <div class="form-group">
                                            <label>File upload</label>
                                            <input type="file" name="company_logo" class="file-upload-default">
                                            <div class="input-group col-xs-12">
                                                <input type="text" class="form-control file-upload-info" disabled
                                                    placeholder="Upload Image">
                                                <span class="input-group-append">
                                                    <button class="file-upload-browse btn btn-primary"
                                                        type="button">Upload</button>
                                                </span>
                                            </div>
                                        </div>

                                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                        <button class="btn btn-light">Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content-wrapper ends -->
                <!-- partial:partials/_footer.html -->
                <?php include "includes/footer.php";?>
                <!-- partial -->
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <?php include "includes/common-js.php";?>
    <!-- End custom js for this page-->
    <script>

        $(document).ready(function () {

            var isValid = false;

            $("#gbSett").on("submit", function (e) {
                e.preventDefault();

                var org_name = $("#org_name").val();
                var org_info = $("#org_info").val();
                var admin_email = $("#admin_email").val();
                var site_url = $("#site_url").val();
                var contact_no = $("#contact_no").val();

                var settingsData = new FormData();
                settingsData.append("org_name", org_name);
                settingsData.append("org_info", org_info);
                settingsData.append("admin_email", admin_email);
                settingsData.append("site_url", site_url);
                settingsData . append("contact_no", contact_no);
                settingsData.append("company_logo", $('input[type=file]')[0].files[0]);

                isvalid  = checkFormStatus("gbSett");

                if(isvalid)
                {
                $.ajax({
                    "type": "POST",
                    "url": "ajax/update-settings.php",
                    "data": settingsData,
                    dataType: "html",
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        $("#loading-image").show();
                        $("#overlay").show();
                    },
                    success: function (response) {
                        $("#loading-image").hide();
                         $("#overlay").hide();
                        if (response == 0) {
                            cuteToast({
                                type: "success",
                                message: "Settings updated successfully...",
                                timer: 3000
                            });
                            setTimeout(function () {
                                location.href = "global-settings.php"
                            }, 3000);
                        } else if (response == 3) {
                            cuteToast({
                                type: "warning",
                                message: "Invalid file type, please choose another file.",
                                timer: 3000
                            });
                        } else {
                            cuteToast({
                                type: "error",
                                message: "Something went wrong...",
                                timer: 3000
                            });
                        }
                    }
                })
            });
          }
        });
    </script>

</body>

</html>