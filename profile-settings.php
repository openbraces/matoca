<?php
include "application-top.php";

if (!isset($_SESSION["email"])) {
    header("location:index.php");
    exit();
}

$pf_sql = "select * from mtc_users where user_id =" . $_SESSION["user_id"];
$pf_res = mysqli_query($con, $pf_sql);
$pf_row = mysqli_fetch_array($pf_res);

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php echo $gbl_row["org_name"]; ?></title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/feather/feather.css">
  <link rel="stylesheet" href="vendors/ti-icons/css/themify-icons.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <link rel="stylesheet" href="vendors/select2/select2.min.css">
  <link rel="stylesheet" href="vendors/select2-bootstrap-theme/select2-bootstrap.min.css">
  <link rel="stylesheet" href="js/cute-alert-master/alert-style.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/vertical-layout-light/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
  <?php include "includes/header.php";?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
    <?php include "includes/right-sidebar.php";?>
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
    <?php include "includes/sidebar-menu.php";?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
           <?php create_breadcrumb();?>
          <div class="row">
            <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Profile Settings</h4>
                  <p class="card-description">
                    Personal info
                  </p>
                  <form class="forms-sample" id="pfForm" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                      <label for="username">Name</label>
                      <input type="text" name="username" value="<?php echo $pf_row["username"]; ?>" required class="form-control" id="username" placeholder="Name">
                    </div>

                   <div class="form-group">
                      <label for="password">Password</label>
                      <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                    </div>

                    <div class="form-group">
                      <label for="password">Confirm Password</label>
                      <input type="password" name="con_password" class="form-control" id="con_password" placeholder="Confirm Password">
                    </div>

                    <div class="form-group">
                      <label>Existing Image</label>
                      <div class="input-group col-xs-12">
                           <img src="upload_user_image/<?php echo $pf_row["proflle_image"]; ?>" class="rounded float-left" alt="<?php echo $pf_row["username"]; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label>File upload</label>
                      <input type="file" name="proflle_image" class="file-upload-default">
                      <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                        <span class="input-group-append">
                          <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                        </span>
                      </div>
                    </div>

                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button class="btn btn-light">Cancel</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <?php include "includes/footer.php";?>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <?php include "includes/common-js.php";?>
  <!-- End custom js for this page-->
  <script>

   $(document).ready(function() {

      var isValid = false;

       $("#pfForm").on("submit", function(e) {
           e.preventDefault();

           var username = $("#username").val();
           var password = $("#password").val();
           var con_password = $("#con_password").val();

           var pData = new FormData();
           pData.append("username", username);
           if(con_password !== "" || con_password !== null || typeof (con_password) !== undefined) {
           pData.append("con_password", con_password);
           }
           pData.append("proflle_image", $('input[type=file]')[0].files[0]);

           isvalid  = checkFormStatus("pfForm");

          if(password !== con_password) {
               cuteToast({
              type: "warning",
              title: "Invalid Password",
              message: "Password does not match...",
              buttonText: "Okay"
             });
           } 
           
           if(isvalid)
           {  
           $.ajax({
               type : "POST",
               url    : "ajax/update-profile.php?user_id="+ <?php echo $_SESSION["user_id"]; ?>,
               data : pData,
              dataType: "html",
              contentType: false,
              cache: false,
              processData: false,
              beforeSend: function() {
                  $("#loading-image").show();
               },
              success: function(response) {
                  if (response == 0) {
                    cuteToast({
                    type: "success",
                    message: "Profile updated successfully...",
                    timer: 3000
                    });
                    setTimeout(function() {
                        location.href = "profile-settings.php"
                    }, 3000);
                  } else if (response == 3) {
                     cuteToast({
                      type: "warning",
                      message: "Invalid file type, please choose another file.",
                      timer: 3000
                    });
                 } else {
                   cuteToast({
                    type: "error",
                    message: "Something went wrong...",
                    timer: 3000
                    });
                }
               },
               error : function(error) {
                 alert(error);
               }
           })
          }
       });
   });
  </script>

</body>
</html>
