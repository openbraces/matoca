-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 13, 2021 at 07:56 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mtc`
--

-- --------------------------------------------------------

--
-- Table structure for table `mtc_global_settings`
--

CREATE TABLE `mtc_global_settings` (
  `setting_id` int(11) NOT NULL,
  `org_name` varchar(255) NOT NULL,
  `org_info` varchar(255) NOT NULL,
  `site_url` varchar(255) NOT NULL,
  `contact_no` varchar(50) NOT NULL,
  `company_logo` varchar(255) NOT NULL,
  `admin_email` varchar(120) NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mtc_global_settings`
--

INSERT INTO `mtc_global_settings` (`setting_id`, `org_name`, `org_info`, `site_url`, `contact_no`, `company_logo`, `admin_email`, `last_updated`) VALUES
(1, 'Mart To Cart', 'No: 13, Kamban Street, Krishnagiri                                           Tamil Nadu, India - 123456.', 'http://openbraces.in/mtc', '9000000001', '1631820152logo.svg', 'dev@openbraces.in', '2021-09-21 00:44:55');

-- --------------------------------------------------------

--
-- Table structure for table `mtc_invoices`
--

CREATE TABLE `mtc_invoices` (
  `order_id` int(11) NOT NULL,
  `invoiceNo` varchar(180) NOT NULL,
  `customerName` varchar(120) DEFAULT NULL,
  `customerContact` varchar(80) NOT NULL,
  `customerAddress` varchar(200) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `invoice_total` decimal(10,2) DEFAULT NULL,
  `invoice_subtotal` decimal(10,2) DEFAULT NULL,
  `tax` decimal(10,2) DEFAULT NULL,
  `tax_percent` decimal(10,2) NOT NULL,
  `amount_paid` decimal(10,2) DEFAULT NULL,
  `amount_due` decimal(10,2) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `uuid` varchar(75) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtc_invoices`
--

INSERT INTO `mtc_invoices` (`order_id`, `invoiceNo`, `customerName`, `customerContact`, `customerAddress`, `client_id`, `invoice_total`, `invoice_subtotal`, `tax`, `tax_percent`, `amount_paid`, `amount_due`, `created_on`, `updated`, `uuid`) VALUES
(1, '', 'test', '', '', 1, '46800.00', '45000.00', '1800.00', '0.00', '7767.00', '39033.00', '2021-08-10 15:01:07', NULL, '6131eb5b1b56f'),
(2, '', 'test', '', '', 1, '46350.00', '45000.00', '1350.00', '0.00', '233.00', '46117.00', '2021-09-03 15:07:14', NULL, '6131eccaed52d'),
(3, '', 'test', '', '', 1, '46350.00', '45000.00', '1350.00', '0.00', '44342.00', '2008.00', '2021-09-03 15:33:32', NULL, '6131f2f4ab011'),
(4, '', 'test', '', '', 1, '46350.00', '45000.00', '1350.00', '0.00', '43424.00', '2926.00', '2021-09-03 15:39:54', NULL, '6131f472212de'),
(5, '', 'test', '', '', 1, '46350.00', '45000.00', '1350.00', '0.00', '3333.00', '43017.00', '2021-09-03 15:47:38', NULL, '6131f64201e9a'),
(6, '', 'test', '', '', 1, '46350.00', '45000.00', '1350.00', '0.00', '3333.00', '43017.00', '2021-09-03 15:48:51', NULL, '6131f68ba2e3e'),
(7, '', 'test', '', '', 1, '59850.00', '45000.00', '14850.00', '0.00', '43324.00', '16526.00', '2021-09-03 15:53:06', NULL, '6131f78ad431e'),
(8, '', 'test', '', '', 1, '50400.00', '45000.00', '5400.00', '12.00', '23333.00', '27067.00', '2021-09-04 00:58:51', NULL, '61327773cd7e7'),
(9, '', 'test', '', '', 1, '45900.00', '45000.00', '900.00', '2.00', '32434.00', '13466.00', '2021-09-04 01:15:54', NULL, '61327b721065b'),
(10, '', 'test', '', '', 1, '54450.00', '45000.00', '9450.00', '21.00', '32434.00', '22016.00', '2021-09-04 01:21:28', NULL, '61327cc04c1f4'),
(11, '', 'test', '', '', 1, '45900.00', '45000.00', '900.00', '2.00', '3344.00', '42556.00', '2021-09-04 01:22:13', NULL, '61327ced33719'),
(12, '', 'test', '', '', 1, '45900.00', '45000.00', '900.00', '2.00', '3344.00', '42556.00', '2021-09-04 01:24:48', NULL, '61327d882ecc0'),
(13, '', 'test', '', '', 1, '45900.00', '45000.00', '900.00', '2.00', '343.00', '45557.00', '2021-09-04 01:26:40', NULL, '61327df8059ea'),
(14, '', 'test', '', '', 1, '47025.00', '45000.00', '2025.00', '4.50', '41223.00', '5802.00', '2021-09-04 01:44:18', NULL, '6132821a87733'),
(15, '', 'test', '', '', 1, '47025.00', '45000.00', '2025.00', '4.50', '41223.00', '5802.00', '2021-09-04 01:55:41', NULL, '613284c534911'),
(16, 'MTC-0000001', 'test', '', '', 1, '45900.00', '45000.00', '900.00', '2.00', '21212.00', '24688.00', '2021-09-04 02:12:46', NULL, '613288c6a0850'),
(17, 'MTC-0000002', 'test', '', '', 1, '45900.00', '45000.00', '900.00', '2.00', '23223.00', '22677.00', '2021-09-04 02:15:34', NULL, '6132896e36a09'),
(18, 'MTC-00018', 'test', '', '', 1, '46350.00', '45000.00', '1350.00', '3.00', '3432.00', '42918.00', '2021-09-04 12:56:17', NULL, '61331f996432a'),
(19, 'MTC-P00001', 'test', '', '', 1, '45675.00', '45000.00', '675.00', '1.50', '45675.00', '0.00', '2021-09-04 14:25:14', NULL, '613334727b57f'),
(20, 'MTC-00020', 'test', '', '', 1, '2436000.00', '2400000.00', '36000.00', '1.50', '2345644.00', '90356.00', '2021-09-04 16:17:09', NULL, '61334ead18d1a'),
(21, 'MTC-00021', 'Smith', '9033344333', 'test address 1', 1, '252000.00', '240000.00', '12000.00', '5.00', '200000.00', '52000.00', '2021-09-05 20:12:48', NULL, '6134d7680d0fd'),
(22, 'MTC-00022', 'Jason', 'test12@mailinator.com', 'Backers lane', 1, '244400.00', '235000.00', '9400.00', '4.00', '200000.00', '44400.00', '2021-09-05 20:16:16', NULL, '6134d8385dfe8'),
(23, 'MTC-00023', 'test', 'test', 'test add', 1, '144200.00', '140000.00', '4200.00', '3.00', '32000.00', '112200.00', '2021-09-05 20:32:16', NULL, '6134dbf8e66aa'),
(24, 'MTC-00023', 'test', 'test', 'test add', 1, '144200.00', '140000.00', '4200.00', '3.00', '32000.00', '112200.00', '2021-09-05 20:33:49', NULL, '6134dc55a7ab5'),
(25, 'MTC-00025', 'sdad', 'dsaa', 'sas', 1, '247200.00', '240000.00', '7200.00', '3.00', '32323.00', '214877.00', '2021-09-05 20:43:37', NULL, '6134dea198f03'),
(26, 'MTC-00026', 'Pual', 'test123@mailinator.com', 'test', 1, '246750.00', '235000.00', '11750.00', '5.00', '240000.00', '6750.00', '2021-09-05 20:49:31', NULL, '6134e003cfde4'),
(27, 'MTC-00027', 'test', '4242434434', 'test', 1, '51030.00', '48600.00', '2430.00', '5.00', '44343.00', '6687.00', '2021-09-11 00:27:50', NULL, '613baaaeb91df'),
(28, 'MTC-00028', 'John', '2313323133', 'test', 1, '144905.17', '140005.00', '4900.18', '3.50', '84323.00', '60582.17', '2021-09-18 17:29:19', NULL, '6145d49750139');

-- --------------------------------------------------------

--
-- Table structure for table `mtc_invoice_details`
--

CREATE TABLE `mtc_invoice_details` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_sku` varchar(100) NOT NULL,
  `product_name` varchar(250) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtc_invoice_details`
--

INSERT INTO `mtc_invoice_details` (`id`, `invoice_id`, `product_id`, `product_sku`, `product_name`, `quantity`, `price`) VALUES
(1, 1, 1, '123', 'Nokia N-73', 1, '45000.00'),
(2, 2, 1, '123', 'Nokia N-73', 1, '45000.00'),
(3, 3, 1, '123', 'Nokia N-73', 1, '45000.00'),
(4, 4, 1, '123', 'Nokia N-73', 1, '45000.00'),
(5, 5, 1, '123', 'Nokia N-73', 1, '45000.00'),
(6, 6, 1, '123', 'Nokia N-73', 1, '45000.00'),
(7, 7, 1, '123', 'Nokia N-73', 1, '45000.00'),
(8, 8, 1, '123', 'Nokia N-73', 1, '45000.00'),
(9, 9, 1, '123', 'Nokia N-73', 1, '45000.00'),
(10, 10, 1, '123', 'Nokia N-73', 1, '45000.00'),
(11, 11, 1, '123', 'Nokia N-73', 1, '45000.00'),
(12, 12, 1, '123', 'Nokia N-73', 1, '45000.00'),
(13, 13, 1, '123', 'Nokia N-73', 1, '45000.00'),
(14, 14, 1, '123', 'Nokia N-73', 1, '45000.00'),
(15, 15, 1, '123', 'Nokia N-73', 1, '45000.00'),
(16, 16, 1, '123', 'Nokia N-73', 1, '45000.00'),
(17, 17, 1, '123', 'Nokia N-73', 1, '45000.00'),
(18, 18, 1, '123', 'Nokia N-73', 1, '45000.00'),
(19, 19, 1, '123', 'Nokia N-73', 1, '45000.00'),
(20, 20, 1, '123', 'Nokia N-73', 10, '450000.00'),
(21, 21, 1, '123', 'Nokia N-73', 2, '45000.00'),
(22, 22, 2, '456', 'N-90', 2, '50000.00'),
(23, 23, 1, '123', 'Nokia N-73', 2, '45000.00'),
(24, 23, 2, '456', 'N-90', 1, '50000.00'),
(25, 24, 1, '123', 'Nokia N-73', 2, '45000.00'),
(26, 24, 2, '456', 'N-90', 1, '50000.00'),
(27, 25, 1, '123', 'Nokia N-73', 2, '45000.00'),
(28, 25, 2, '456', 'N-90', 3, '50000.00'),
(29, 26, 1, '123', 'Nokia N-73', 3, '45000.00'),
(30, 26, 2, '456', 'N-90', 2, '50000.00'),
(31, 27, 5, '789', 'Test Product', 1, '3600.00'),
(32, 27, 1, '123', 'Nokia N-73', 1, '45000.00'),
(33, 28, 1, '123', 'Nokia N-73', 2, '45000.00'),
(34, 28, 2, '456', 'N-90', 1, '50005.00');

-- --------------------------------------------------------

--
-- Table structure for table `mtc_orders`
--

CREATE TABLE `mtc_orders` (
  `purchase_order_id` int(11) NOT NULL,
  `invoiceNo` varchar(180) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `customerName` varchar(120) DEFAULT NULL,
  `customerContact` varchar(80) NOT NULL,
  `customerAddress` varchar(200) NOT NULL,
  `invoice_total` decimal(10,2) DEFAULT NULL,
  `invoice_subtotal` decimal(10,2) DEFAULT NULL,
  `tax` decimal(10,2) DEFAULT NULL,
  `tax_percent` decimal(10,2) NOT NULL,
  `amount_paid` decimal(10,2) DEFAULT NULL,
  `amount_due` decimal(10,2) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `uuid` varchar(75) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtc_orders`
--

INSERT INTO `mtc_orders` (`purchase_order_id`, `invoiceNo`, `client_id`, `customerName`, `customerContact`, `customerAddress`, `invoice_total`, `invoice_subtotal`, `tax`, `tax_percent`, `amount_paid`, `amount_due`, `created_on`, `updated`, `uuid`) VALUES
(1, 'MTC-P00001', 1, 'test', '', '', '913500.00', '900000.00', '13500.00', '1.50', '913500.00', '0.00', '2021-08-10 14:40:22', NULL, '613337feeedca'),
(2, 'MTC-P00002', 1, 'test', '', '', '547560.00', '540000.00', '7560.00', '1.40', '540000.00', '7560.00', '2021-09-04 14:43:28', NULL, '613338b8effb8'),
(3, 'MTC-P00003', 1, 'test', '', '', '999900.00', '990000.00', '9900.00', '1.00', '990000.00', '9900.00', '2021-09-04 14:45:21', NULL, '6133392983ee4'),
(4, 'MTC-P00004', 1, 'test', '', '', '550800.00', '540000.00', '10800.00', '2.00', '32323.00', '518477.00', '2021-09-04 15:38:45', NULL, '613345ad8d610'),
(5, 'MTC-P00005', 1, 'test', '', '', '545400.00', '540000.00', '5400.00', '1.00', '122.00', '545278.00', '2021-09-04 15:40:13', NULL, '613346056bc74'),
(6, 'MTC-P00006', 1, 'test', '', '', '45450.00', '45000.00', '450.00', '1.00', '2323.00', '43127.00', '2021-09-04 15:43:39', NULL, '613346d3460ca'),
(7, 'MTC-P00007', 1, 'test', '', '', '50400.00', '45000.00', '5400.00', '12.00', '12.00', '50388.00', '2021-09-04 15:44:46', NULL, '613347166053d'),
(8, 'MTC-P00008', 1, 'test', '', '', '46350.00', '45000.00', '1350.00', '3.00', '3433.00', '42917.00', '2021-07-05 15:45:54', NULL, '6133475af333e'),
(9, 'MTC-P00009', 1, 'test', '', '', '45450.00', '45000.00', '450.00', '1.00', '233.00', '45217.00', '2021-09-04 15:47:06', NULL, '613347a2de196'),
(10, 'MTC-P00010', 1, 'test', '', '', '46350.00', '45000.00', '1350.00', '3.00', '23121.00', '23229.00', '2021-09-04 15:51:12', NULL, '61334898e7088'),
(11, 'MTC-P00011', 1, 'test', '', '', '556200.00', '540000.00', '16200.00', '3.00', '232323.00', '323877.00', '2021-09-04 16:10:34', NULL, '61334d225b309'),
(12, 'MTC-P00012', 1, 'test', '', '', '1225700.00', '1190000.00', '35700.00', '3.00', '32323.00', '1193377.00', '2021-09-04 16:27:47', NULL, '6133512b41b4e'),
(13, 'MTC-P00013', 1, 'test', '', '', '1621800.00', '1590000.00', '31800.00', '2.00', '32312.00', '1589488.00', '2021-09-05 00:41:47', NULL, '6133c4f3b5624'),
(14, 'MTC-P00014', 1, 'test', '', '', '97850.00', '95000.00', '2850.00', '3.00', '3234.00', '94616.00', '2021-09-05 00:43:52', NULL, '6133c5708be50'),
(15, 'MTC-P00014', 1, 'test', '', '', '97850.00', '95000.00', '2850.00', '3.00', '3234.00', '94616.00', '2021-09-05 00:44:29', NULL, '6133c5959773f'),
(16, 'MTC-P00016', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '2233.00', '94667.00', '2021-09-05 00:46:11', NULL, '6133c5fb3ce7e'),
(17, 'MTC-P00016', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '2233.00', '94667.00', '2021-09-05 00:46:31', NULL, '6133c60f2ae4b'),
(18, 'MTC-P00018', 1, 'test', '', '', '115900.00', '95000.00', '20900.00', '22.00', '2312.00', '113588.00', '2021-09-05 00:49:57', NULL, '6133c6dd3b3f4'),
(19, 'MTC-P00019', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '43233.00', '53667.00', '2021-09-05 00:50:11', NULL, '6133c6ebf0975'),
(20, 'MTC-P00019', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '43233.00', '53667.00', '2021-09-05 00:51:03', NULL, '6133c71f09392'),
(21, 'MTC-P00021', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '42424.00', '54476.00', '2021-09-05 00:51:22', NULL, '6133c732822fa'),
(22, 'MTC-P00022', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '32344.00', '64556.00', '2021-09-05 00:52:14', NULL, '6133c76692e2a'),
(23, 'MTC-P00022', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '32344.00', '64556.00', '2021-09-05 00:54:48', NULL, '6133c8009dcc0'),
(24, 'MTC-P00024', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '32323.00', '64577.00', '2021-09-05 00:55:35', NULL, '6133c82fbc89d'),
(25, 'MTC-P00025', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '3244.00', '93656.00', '2021-09-05 01:06:27', NULL, '6133cabbcbbe4'),
(26, 'MTC-P00026', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '3212.00', '93688.00', '2021-09-05 01:08:50', NULL, '6133cb4a78eda'),
(27, 'MTC-P00026', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '3212.00', '93688.00', '2021-09-05 01:09:11', NULL, '6133cb5f3eb68'),
(28, 'MTC-P00026', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '3212.00', '93688.00', '2021-09-05 01:09:45', NULL, '6133cb8104804'),
(29, 'MTC-P00029', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '2112.00', '94788.00', '2021-09-05 01:11:06', NULL, '6133cbd212193'),
(30, 'MTC-P00030', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '32324.00', '64576.00', '2021-09-05 01:14:01', NULL, '6133cc8119a47'),
(31, 'MTC-P00030', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '32324.00', '64576.00', '2021-09-05 01:15:20', NULL, '6133ccd0d388d'),
(32, 'MTC-P00030', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '32324.00', '64576.00', '2021-09-05 01:16:56', NULL, '6133cd30c4b92'),
(33, 'MTC-P00030', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '32324.00', '64576.00', '2021-09-05 01:17:28', NULL, '6133cd507e3fd'),
(34, 'MTC-P00030', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '32324.00', '64576.00', '2021-09-05 01:18:47', NULL, '6133cd9f8d4ab'),
(35, 'MTC-P00035', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '1122.00', '95778.00', '2021-09-05 01:19:07', NULL, '6133cdb3afc3d'),
(36, 'MTC-P00035', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '1122.00', '95778.00', '2021-09-05 01:19:40', NULL, '6133cdd4f3ae0'),
(37, 'MTC-P00037', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '22321.00', '74579.00', '2021-09-05 01:20:07', NULL, '6133cdef0f5a9'),
(38, 'MTC-P00038', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '2323.00', '94577.00', '2021-09-05 01:21:19', NULL, '6133ce3790099'),
(39, 'MTC-P00039', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '32322.00', '64578.00', '2021-09-05 01:22:13', NULL, '6133ce6db9ff1'),
(40, 'MTC-P00039', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '32322.00', '64578.00', '2021-09-05 01:24:56', NULL, '6133cf1007e4c'),
(41, 'MTC-P00041', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '3443.00', '93457.00', '2021-09-05 01:26:26', NULL, '6133cf6a2eaee'),
(42, 'MTC-P00042', 1, 'test', '', '', '2544900.00', '2495000.00', '49900.00', '2.00', '2500000.00', '44900.00', '2021-09-05 01:28:28', NULL, '6133cfe40bf6a'),
(43, 'MTC-P00043', 1, 'test', '', '', '1060800.00', '1040000.00', '20800.00', '2.00', '234404.00', '826396.00', '2021-09-05 01:32:03', NULL, '6133d0bbab95e'),
(44, 'MTC-P00044', 1, 'test', '', '', '1310700.00', '1285000.00', '25700.00', '2.00', '321323.00', '989377.00', '2021-09-05 01:34:54', NULL, '6133d1665bde0'),
(45, 'MTC-P00044', 1, 'test', '', '', '1310700.00', '1285000.00', '25700.00', '2.00', '321323.00', '989377.00', '2021-09-05 01:38:20', NULL, '6133d2343860e'),
(46, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 01:39:16', NULL, '6133d26c998d8'),
(47, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 01:39:45', NULL, '6133d28994f09'),
(48, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 01:40:33', NULL, '6133d2b9d73d0'),
(49, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 01:41:39', NULL, '6133d2fb27de2'),
(50, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 01:42:04', NULL, '6133d31441bd8'),
(51, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 01:42:21', NULL, '6133d325b87eb'),
(52, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 01:47:12', NULL, '6133d448f04c7'),
(53, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 01:47:21', NULL, '6133d451b7a91'),
(54, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 01:47:52', NULL, '6133d470bb9a7'),
(55, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 01:48:19', NULL, '6133d48b97f61'),
(56, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 01:48:51', NULL, '6133d4abdb8f9'),
(57, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 01:49:05', NULL, '6133d4b908776'),
(58, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 01:50:52', NULL, '6133d524b656d'),
(59, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 01:57:15', NULL, '6133d6a3593bd'),
(60, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 01:58:32', NULL, '6133d6f0a69b3'),
(61, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 02:00:05', NULL, '6133d74da6191'),
(62, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 02:00:16', NULL, '6133d758ee4ec'),
(63, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 02:00:44', NULL, '6133d774f0c0e'),
(64, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 02:01:01', NULL, '6133d7850204d'),
(65, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 02:01:27', NULL, '6133d79f4176d'),
(66, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 02:01:36', NULL, '6133d7a8d9450'),
(67, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 02:01:47', NULL, '6133d7b383e9a'),
(68, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 02:04:34', NULL, '6133d85a4adc2'),
(69, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 02:05:01', NULL, '6133d8756e13a'),
(70, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 02:06:11', NULL, '6133d8bb008e0'),
(71, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 02:06:24', NULL, '6133d8c842302'),
(72, 'MTC-P00046', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '321323.00', '-224423.00', '2021-09-05 02:06:53', NULL, '6133d8e5106bc'),
(73, 'MTC-P00073', 1, 'test', '', '', '97850.00', '95000.00', '2850.00', '3.00', '44232.00', '53618.00', '2021-09-05 11:27:14', NULL, '61345c3a8214f'),
(74, 'MTC-P00074', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '23233.00', '73667.00', '2021-09-05 11:29:41', NULL, '61345ccd630be'),
(75, 'MTC-P00074', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '23233.00', '73667.00', '2021-09-05 11:30:50', NULL, '61345d12d0f98'),
(76, 'MTC-P00074', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '23233.00', '73667.00', '2021-09-05 11:31:26', NULL, '61345d36ed05b'),
(77, 'MTC-P00074', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '23233.00', '73667.00', '2021-09-05 11:31:45', NULL, '61345d494004f'),
(78, 'MTC-P00074', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '23233.00', '73667.00', '2021-09-05 11:31:55', NULL, '61345d537aef4'),
(79, 'MTC-P00074', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '23233.00', '73667.00', '2021-09-05 11:32:16', NULL, '61345d683df8d'),
(80, 'MTC-P00074', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '23233.00', '73667.00', '2021-09-05 11:32:55', NULL, '61345d8f45da0'),
(81, 'MTC-P00081', 1, 'test', '', '', '96900.00', '95000.00', '1900.00', '2.00', '82321.00', '14579.00', '2021-09-05 11:34:02', NULL, '61345dd22f350'),
(82, 'MTC-P00082', 1, 'test', '', '', '97850.00', '95000.00', '2850.00', '3.00', '86433.00', '11417.00', '2021-09-05 11:37:07', NULL, '61345e8b3466f'),
(83, 'MTC-P00083', 1, 'test', '', '', '714000.00', '700000.00', '14000.00', '2.00', '653335.00', '60665.00', '2021-09-05 11:39:46', NULL, '61345f2a24a36'),
(84, 'MTC-P00084', 1, 'test', '', '', '739500.00', '725000.00', '14500.00', '2.00', '653342.00', '86158.00', '2021-09-05 11:51:03', NULL, '613461cfb4716'),
(85, 'MTC-P00085', 1, 'test', '', '', '520200.00', '510000.00', '10200.00', '2.00', '345533.00', '174667.00', '2021-09-05 11:53:18', NULL, '6134625649a87'),
(86, 'MTC-P00086', 1, 'test', '', '', '45900.00', '45000.00', '900.00', '2.00', '222.00', '45678.00', '2021-09-05 12:33:06', NULL, '61346baa3fd92'),
(87, 'MTC-P00087', 1, 'John Doe', 'jd@mailinator.com', 'test address', '1433100.00', '1405000.00', '28100.00', '2.00', '1200000.00', '233100.00', '2021-09-05 13:16:13', NULL, '613475c5410d4'),
(88, 'MTC-P00088', 1, 'Adrian Smith', 'ad@mialinator.com', 'Addess line one', '1064056.00', '950050.00', '114006.00', '12.00', '999999.00', '64057.00', '2021-09-20 00:47:55', NULL, '61478ce3c8da4'),
(89, 'MTC-P00089', 1, 'John', '32132321333', 'test', '258511.00', '235010.00', '23501.00', '10.00', '240006.00', '18505.00', '2021-09-21 00:19:18', NULL, '6148d7ae4ad1c'),
(90, 'MTC-P00090', 1, 'test', '23423232323', 'test', '2122.00', '2122.00', '0.00', '0.00', '2000.00', '122.00', '2021-10-07 00:38:45', NULL, '615df43d84a0e');

-- --------------------------------------------------------

--
-- Table structure for table `mtc_order_details`
--

CREATE TABLE `mtc_order_details` (
  `id` int(11) NOT NULL,
  `purchase_invoice_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_sku` varchar(100) NOT NULL,
  `product_name` varchar(250) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mtc_order_details`
--

INSERT INTO `mtc_order_details` (`id`, `purchase_invoice_id`, `product_id`, `product_sku`, `product_name`, `quantity`, `price`) VALUES
(1, 3, 1, '123', 'Nokia N-73', 22, '990000.00'),
(2, 4, 1, '123', 'Nokia N-73', 12, '540000.00'),
(3, 5, 1, '123', 'Nokia N-73', 12, '540000.00'),
(4, 6, 1, '123', 'Nokia N-73', 1, '45000.00'),
(5, 7, 1, '123', 'Nokia N-73', 1, '45000.00'),
(6, 8, 1, '123', 'Nokia N-73', 1, '45000.00'),
(7, 9, 1, '123', 'Nokia N-73', 1, '45000.00'),
(8, 10, 1, '123', 'Nokia N-73', 1, '45000.00'),
(9, 11, 1, '123', 'Nokia N-73', 12, '540000.00'),
(10, 25, 2, '456', 'N-90', 1, '50000.00'),
(11, 40, 2, '456', 'N-90', 1, '50000.00'),
(12, 41, 1, '123', 'Nokia N-73', 1, '45000.00'),
(13, 41, 2, '456', 'N-90', 1, '50000.00'),
(14, 42, 1, '123', 'Nokia N-73', 1, '45000.00'),
(15, 42, 2, '456', 'N-90', 49, '50000.00'),
(16, 43, 1, '123', 'Nokia N-73', 12, '45000.00'),
(17, 43, 2, '456', 'N-90', 10, '50000.00'),
(18, 44, 1, '123', 'Nokia N-73', 13, '45000.00'),
(19, 44, 2, '456', 'N-90', 14, '50000.00'),
(20, 45, 1, '123', 'Nokia N-73', 13, '45000.00'),
(21, 45, 2, '456', 'N-90', 14, '50000.00'),
(22, 46, 1, '123', 'Nokia N-73', 1, '45000.00'),
(23, 46, 2, '456', 'N-90', 1, '50000.00'),
(24, 47, 1, '123', 'Nokia N-73', 1, '45000.00'),
(25, 47, 2, '456', 'N-90', 1, '50000.00'),
(26, 48, 1, '123', 'Nokia N-73', 1, '45000.00'),
(27, 48, 2, '456', 'N-90', 1, '50000.00'),
(28, 49, 1, '123', 'Nokia N-73', 1, '45000.00'),
(29, 49, 2, '456', 'N-90', 1, '50000.00'),
(30, 50, 1, '123', 'Nokia N-73', 1, '45000.00'),
(31, 50, 2, '456', 'N-90', 1, '50000.00'),
(32, 51, 1, '123', 'Nokia N-73', 1, '45000.00'),
(33, 51, 2, '456', 'N-90', 1, '50000.00'),
(34, 52, 1, '123', 'Nokia N-73', 1, '45000.00'),
(35, 52, 2, '456', 'N-90', 1, '50000.00'),
(36, 53, 1, '123', 'Nokia N-73', 1, '45000.00'),
(37, 53, 2, '456', 'N-90', 1, '50000.00'),
(38, 54, 1, '123', 'Nokia N-73', 1, '45000.00'),
(39, 54, 2, '456', 'N-90', 1, '50000.00'),
(40, 55, 1, '123', 'Nokia N-73', 1, '45000.00'),
(41, 55, 2, '456', 'N-90', 1, '50000.00'),
(42, 56, 1, '123', 'Nokia N-73', 1, '45000.00'),
(43, 56, 2, '456', 'N-90', 1, '50000.00'),
(44, 57, 1, '123', 'Nokia N-73', 1, '45000.00'),
(45, 57, 2, '456', 'N-90', 1, '50000.00'),
(46, 58, 1, '123', 'Nokia N-73', 1, '45000.00'),
(47, 58, 2, '456', 'N-90', 1, '50000.00'),
(48, 59, 1, '123', 'Nokia N-73', 1, '45000.00'),
(49, 59, 2, '456', 'N-90', 1, '50000.00'),
(50, 60, 1, '123', 'Nokia N-73', 1, '45000.00'),
(51, 60, 2, '456', 'N-90', 1, '50000.00'),
(52, 61, 1, '123', 'Nokia N-73', 1, '45000.00'),
(53, 61, 2, '456', 'N-90', 1, '50000.00'),
(54, 62, 1, '123', 'Nokia N-73', 1, '45000.00'),
(55, 62, 2, '456', 'N-90', 1, '50000.00'),
(56, 63, 1, '123', 'Nokia N-73', 1, '45000.00'),
(57, 63, 2, '456', 'N-90', 1, '50000.00'),
(58, 64, 1, '123', 'Nokia N-73', 1, '45000.00'),
(59, 64, 2, '456', 'N-90', 1, '50000.00'),
(60, 65, 1, '123', 'Nokia N-73', 1, '45000.00'),
(61, 65, 2, '456', 'N-90', 1, '50000.00'),
(62, 66, 1, '123', 'Nokia N-73', 1, '45000.00'),
(63, 66, 2, '456', 'N-90', 1, '50000.00'),
(64, 67, 1, '123', 'Nokia N-73', 1, '45000.00'),
(65, 67, 2, '456', 'N-90', 1, '50000.00'),
(66, 68, 1, '123', 'Nokia N-73', 1, '45000.00'),
(67, 68, 2, '456', 'N-90', 1, '50000.00'),
(68, 69, 1, '123', 'Nokia N-73', 1, '45000.00'),
(69, 69, 2, '456', 'N-90', 1, '50000.00'),
(70, 70, 1, '123', 'Nokia N-73', 1, '45000.00'),
(71, 70, 2, '456', 'N-90', 1, '50000.00'),
(72, 71, 1, '123', 'Nokia N-73', 1, '45000.00'),
(73, 71, 2, '456', 'N-90', 1, '50000.00'),
(74, 72, 1, '123', 'Nokia N-73', 1, '45000.00'),
(75, 72, 2, '456', 'N-90', 1, '50000.00'),
(76, 73, 1, '123', 'Nokia N-73', 1, '45000.00'),
(77, 73, 2, '456', 'N-90', 1, '50000.00'),
(78, 74, 1, '123', 'Nokia N-73', 1, '45000.00'),
(79, 74, 2, '456', 'N-90', 1, '50000.00'),
(80, 75, 1, '123', 'Nokia N-73', 1, '45000.00'),
(81, 75, 2, '456', 'N-90', 1, '50000.00'),
(82, 76, 1, '123', 'Nokia N-73', 1, '45000.00'),
(83, 76, 2, '456', 'N-90', 1, '50000.00'),
(84, 77, 1, '123', 'Nokia N-73', 1, '45000.00'),
(85, 77, 2, '456', 'N-90', 1, '50000.00'),
(86, 78, 1, '123', 'Nokia N-73', 1, '45000.00'),
(87, 78, 2, '456', 'N-90', 1, '50000.00'),
(88, 79, 1, '123', 'Nokia N-73', 1, '45000.00'),
(89, 79, 2, '456', 'N-90', 1, '50000.00'),
(90, 80, 1, '123', 'Nokia N-73', 1, '45000.00'),
(91, 80, 2, '456', 'N-90', 1, '50000.00'),
(92, 81, 1, '123', 'Nokia N-73', 1, '45000.00'),
(93, 81, 2, '456', 'N-90', 1, '50000.00'),
(94, 82, 1, '123', 'Nokia N-73', 1, '45000.00'),
(95, 82, 2, '456', 'N-90', 1, '50000.00'),
(96, 83, 1, '123', 'Nokia N-73', 10, '45000.00'),
(97, 83, 2, '456', 'N-90', 5, '50000.00'),
(98, 84, 1, '123', 'Nokia N-73', 5, '45000.00'),
(99, 84, 2, '456', 'N-90', 10, '50000.00'),
(100, 85, 1, '123', 'Nokia N-73', 8, '45000.00'),
(101, 85, 2, '456', 'N-90', 3, '50000.00'),
(102, 86, 1, '123', 'Nokia N-73', 1, '45000.00'),
(103, 87, 1, '123', 'Nokia N-73', 9, '45000.00'),
(104, 87, 2, '456', 'N-90', 20, '50000.00'),
(105, 88, 1, '123', 'Nokia N-73', 10, '45000.00'),
(106, 88, 2, '456', 'N-90', 10, '50005.00'),
(107, 89, 1, '123', 'Nokia N-73', 3, '45000.00'),
(108, 89, 2, '456', 'N-90', 2, '50005.00'),
(109, 90, 6, '312', 'Product test 2', 1, '2122.00');

-- --------------------------------------------------------

--
-- Table structure for table `mtc_products`
--

CREATE TABLE `mtc_products` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_sku_code` varchar(30) NOT NULL,
  `product_desc` varchar(255) NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `product_instock` int(10) NOT NULL,
  `product_price` decimal(10,2) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mtc_products`
--

INSERT INTO `mtc_products` (`product_id`, `product_name`, `product_sku_code`, `product_desc`, `product_image`, `product_instock`, `product_price`, `status`, `added_on`) VALUES
(1, 'Nokia N-73', '123', 'Nice Phone', 'noimage.jpg', 117, '45000.00', 1, '2021-09-01 01:53:29'),
(2, 'N-90', '456', 'Demo content', '163104055613.jpg', 104, '50005.00', 1, '2021-09-04 16:12:39'),
(5, 'Test Product', '789', 'Test content', '16310440096.jpg', 0, '3600.00', 1, '2021-09-08 01:06:04'),
(6, 'Product test 2', '312', 'test', 'noimage.jpg', 2, '2122.00', 0, '2021-09-08 01:08:35');

-- --------------------------------------------------------

--
-- Table structure for table `mtc_users`
--

CREATE TABLE `mtc_users` (
  `user_id` int(10) NOT NULL,
  `username` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `usertype` int(5) NOT NULL,
  `proflle_image` varchar(255) NOT NULL,
  `last_loggedin` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mtc_users`
--

INSERT INTO `mtc_users` (`user_id`, `username`, `email`, `password`, `usertype`, `proflle_image`, `last_loggedin`, `status`, `created_on`) VALUES
(1, 'Jhon Doe', 'admin@admin.com', '7d7c47b2bed4820d77094001207cda38012cdf25', 1, '1630956793face8.jpg', '2021-10-10 06:17:52', 1, '2021-08-31 01:47:11'),
(2, 'Terry Smith', 'jsn@mtc.com', '132c9d1b0fd3687a4a7bdd42a7ca596cddd94ca0', 2, '1631963835face11.jpg', '2021-09-19 19:01:59', 1, '2021-09-06 23:53:16'),
(3, 'test user', 'test123@mailinator.com', 'b2c2a9ca41e220a80237ea3f484b92af0b7c7223', 7, '1631276353face10.jpg', '2021-09-14 19:18:49', 1, '2021-09-10 17:49:13');

-- --------------------------------------------------------

--
-- Table structure for table `mtc_usertypes`
--

CREATE TABLE `mtc_usertypes` (
  `usertype_id` int(11) NOT NULL,
  `usertype_name` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mtc_usertypes`
--

INSERT INTO `mtc_usertypes` (`usertype_id`, `usertype_name`, `status`, `created_on`) VALUES
(1, 'admin', 1, '0000-00-00 00:00:00'),
(2, 'accountant', 1, '0000-00-00 00:00:00'),
(7, 'Editor', 1, '2021-09-10 02:00:06'),
(8, 'Sales Person', 1, '2021-09-11 14:09:50'),
(10, 'test 123', 0, '2021-09-11 14:17:17');

-- --------------------------------------------------------

--
-- Table structure for table `mtc_user_previleges`
--

CREATE TABLE `mtc_user_previleges` (
  `prev_id` int(11) NOT NULL,
  `usertype_id` int(11) NOT NULL,
  `user_module_settings` tinyint(1) NOT NULL DEFAULT 0,
  `product_module_settings` tinyint(1) NOT NULL DEFAULT 0,
  `purchase_module_settings` tinyint(1) NOT NULL DEFAULT 0,
  `invoice_module_settings` tinyint(1) NOT NULL DEFAULT 0,
  `updated_on` datetime NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mtc_user_previleges`
--

INSERT INTO `mtc_user_previleges` (`prev_id`, `usertype_id`, `user_module_settings`, `product_module_settings`, `purchase_module_settings`, `invoice_module_settings`, `updated_on`, `updated_by`) VALUES
(1, 1, 1, 1, 1, 1, '2021-09-10 00:00:00', 1),
(2, 2, 1, 0, 0, 0, '2021-09-10 00:00:00', 1),
(7, 7, 0, 0, 0, 0, '2021-09-10 02:00:06', 1),
(8, 8, 0, 1, 0, 1, '2021-09-11 14:09:50', 1),
(10, 10, 0, 0, 0, 0, '2021-09-11 14:17:18', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mtc_global_settings`
--
ALTER TABLE `mtc_global_settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `mtc_invoices`
--
ALTER TABLE `mtc_invoices`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `mtc_invoice_details`
--
ALTER TABLE `mtc_invoice_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_invoice_details_invoices_idx` (`invoice_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `mtc_orders`
--
ALTER TABLE `mtc_orders`
  ADD PRIMARY KEY (`purchase_order_id`);

--
-- Indexes for table `mtc_order_details`
--
ALTER TABLE `mtc_order_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `purchase_invoice_id` (`purchase_invoice_id`);

--
-- Indexes for table `mtc_products`
--
ALTER TABLE `mtc_products`
  ADD PRIMARY KEY (`product_id`),
  ADD UNIQUE KEY `product_sku_code` (`product_sku_code`);

--
-- Indexes for table `mtc_users`
--
ALTER TABLE `mtc_users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `mtc_users_ibfk_1` (`usertype`);

--
-- Indexes for table `mtc_usertypes`
--
ALTER TABLE `mtc_usertypes`
  ADD PRIMARY KEY (`usertype_id`);

--
-- Indexes for table `mtc_user_previleges`
--
ALTER TABLE `mtc_user_previleges`
  ADD PRIMARY KEY (`prev_id`),
  ADD KEY `usertype_id` (`usertype_id`),
  ADD KEY `updated_by` (`updated_by`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mtc_invoices`
--
ALTER TABLE `mtc_invoices`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `mtc_invoice_details`
--
ALTER TABLE `mtc_invoice_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `mtc_orders`
--
ALTER TABLE `mtc_orders`
  MODIFY `purchase_order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `mtc_order_details`
--
ALTER TABLE `mtc_order_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT for table `mtc_products`
--
ALTER TABLE `mtc_products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `mtc_users`
--
ALTER TABLE `mtc_users`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mtc_usertypes`
--
ALTER TABLE `mtc_usertypes`
  MODIFY `usertype_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `mtc_user_previleges`
--
ALTER TABLE `mtc_user_previleges`
  MODIFY `prev_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `mtc_invoice_details`
--
ALTER TABLE `mtc_invoice_details`
  ADD CONSTRAINT `fk_invoice_details_invoices` FOREIGN KEY (`invoice_id`) REFERENCES `mtc_invoices` (`order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `mtc_invoice_details_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `mtc_products` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mtc_order_details`
--
ALTER TABLE `mtc_order_details`
  ADD CONSTRAINT `mtc_order_details_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `mtc_products` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `mtc_order_details_ibfk_2` FOREIGN KEY (`purchase_invoice_id`) REFERENCES `mtc_orders` (`purchase_order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mtc_users`
--
ALTER TABLE `mtc_users`
  ADD CONSTRAINT `mtc_users_ibfk_1` FOREIGN KEY (`usertype`) REFERENCES `mtc_usertypes` (`usertype_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mtc_user_previleges`
--
ALTER TABLE `mtc_user_previleges`
  ADD CONSTRAINT `mtc_user_previleges_ibfk_1` FOREIGN KEY (`usertype_id`) REFERENCES `mtc_usertypes` (`usertype_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mtc_user_previleges_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `mtc_users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
