<?php
include("application-top.php");

//check if can login again
if(isset($_SESSION['reAttempt'])){
  $now = time();
  if($now >= $_SESSION['reAttempt']){
    unset($_SESSION['attempt']);
    unset($_SESSION['reAttempt']);
  }
}

//set disable if three login attempts has been made
$disable = '';

if(isset($_SESSION['attempt']) && $_SESSION['attempt'] == 1) {
  $_SESSION['error'] = '2 more attempts are remaining.';
} else if(isset($_SESSION['attempt']) && $_SESSION['attempt'] == 2) {
  $_SESSION['error'] = '1 more attempts are remaining.';
} else if (isset($_SESSION['attempt']) && $_SESSION['attempt'] >= 3) {
  $_SESSION['error'] = 'Maximum limit reached. Please try to login after 5 mins.';
  $disable = 'disabled';
}

/* Encryption key geneartion */

$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
$charactersLength = strlen($characters);
$randomString = '';
for ($i = 0; $i < 32; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Skydash Admin</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/feather/feather.css">
  <link rel="stylesheet" href="vendors/ti-icons/css/themify-icons.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <link rel="stylesheet" href="js/cute-alert-master/alert-style.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/vertical-layout-light/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />

</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper  d-flex align-items-stretch auth px-0" style="padding: 0; background: #fff;">
        <div class="row flex-grow">
          <div class="col-lg-6 d-flex align-items-center justify-content-center">
            <div class="auth-form-transparent text-left p-3">
              <div class="brand-logo">
                <img src="images/logo.svg" alt="logo">
              </div>
              <h4>Hello! let's get started</h4>
              <h6 class="font-weight-light">Sign in to continue.</h6>
              <form class="pt-3" method="post" id="loginForm">
                  <input type="hidden" name="cryptoKey" id="cryptoKey" value="<?php echo $randomString; ?>">
                <div class="form-group">
                  <input type="email" name="email" required class="form-control form-control-lg" id="email" placeholder="Username">
                </div>
                <div class="form-group">
                  <input type="password" name="password" required class="form-control form-control-lg" id="password" placeholder="Password">
                </div>
                <div class="mt-3">
                  <input type="submit" <?php echo $disable; ?> name="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" value="SIGN IN">
                </div>
                <div class="my-2 d-flex justify-content-between align-items-center">
                  <div class="form-check">
                    <label class="form-check-label text-muted">
                      <input type="checkbox" name="rememberMe" class="form-check-input" value="1" <?php if (isset($_COOKIE["loggedin_user"])) {?> checked <?php }?> id="rememberMe">
                      Keep me signed in
                    </label>
                  </div>
                  <a href="#" class="auth-link text-black">Forgot password?</a>
                </div>
                <div class="text-center mt-4 font-weight-light">
                  <div class="alert alert-danger text-center" id="error"></div>
                  <?php 
                    if(isset($_SESSION['error'])) {
				        	?>
                    <div class="alert alert-danger text-center" id="failurelogs" style="margin-top:20px;">
                      <?php echo $_SESSION['error']; ?>
                    </div>
				        	<?php
				          	unset($_SESSION['error']);
				         }
                 ?>
                </div>
              </form>
            </div>
          </div>
          <div class="col-lg-6 login-half-bg d-flex flex-row">

            <p class="text-white font-weight-medium text-center flex-grow align-self-end">Copyright © 2021-22  All rights reserved.</p>

          </div>

        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- Loading the common js -->
 <?php include "includes/common-js.php";?>
  <!-- End of loading common js -->

</body>

</html>
