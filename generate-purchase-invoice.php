<?php
ob_start();
include "application-top.php";
include "purchase-invoice.php";

if (!empty($_GET['purchase_invoice_id']) && $_GET['purchase_invoice_id']) {
    $invoiceValues = getInvoice($_GET['purchase_invoice_id']);
    $invoiceItems = getInvoiceItems($_GET['purchase_invoice_id']);
}

$invoiceDate = date("d/M/Y, H:i:s", strtotime($invoiceValues['created_on']));
$output = '';
$output .= '<table width="100%" border="1" cellpadding="5" cellspacing="0">
	<tr>
	<td colspan="2" align="center" style="font-size:18px"><b>Invoice</b></td>
	</tr>
	<tr>
	<td colspan="2">
	<table width="100%" cellpadding="5">
	<tr>
	<td width="65%">
	To,<br />
	<b>RECEIVER (BILL TO)</b><br />
	Name : ' . $invoiceValues['customerName'] . '<br />
	Billing Address : ' . $invoiceValues['customerAddress'] . '<br />
	</td>
	<td width="35%">
	Invoice No. : ' . $invoiceValues['invoiceNo'] . '<br />
	Invoice Date : ' . $invoiceDate . '<br />
	</td>
	</tr>
	</table>
	<br />
	<table width="100%" border="1" cellpadding="5" cellspacing="0">
	<tr>
	<th align="left">Sr No.</th>
	<th align="left">Item Code</th>
	<th align="left">Item Name</th>
	<th align="left">Quantity</th>
	<th align="left">Price</th>
	<th align="left">Actual Amt.</th>
	</tr>';
$count = 0;
foreach ($invoiceItems as $invoiceItem) {
    $count++;
    $output .= '
	<tr>
	<td align="left">' . $count . '</td>
	<td align="left">' . $invoiceItem["product_sku"] . '</td>
	<td align="left">' . $invoiceItem["product_name"] . '</td>
	<td align="left">' . $invoiceItem["quantity"] . '</td>
	<td align="left">' . $invoiceItem["price"] . '</td>
	<td align="left">' . $invoiceItem["quantity"] * $invoiceItem["price"] . '</td>
	</tr>';
}
$output .= '
	<tr>
	<td align="right" colspan="5"><b>Sub Total</b></td>
	<td align="left"><b>' . $invoiceValues['invoice_subtotal'] . '</b></td>
	</tr>
	<tr>
	<td align="right" colspan="5"><b>Tax Rate :</b></td>
	<td align="left">' . $invoiceValues['tax_percent'] . ' %' . '</td>
	</tr>
	<tr>
	<td align="right" colspan="5">Tax Amount: </td>
	<td align="left">' . $invoiceValues['tax'] . '</td>
	</tr>
	<tr>
	<td align="right" colspan="5">Total: </td>
	<td align="left">' . $invoiceValues['invoice_total'] . '</td>
	</tr>
	<tr>
	<td align="right" colspan="5">Amount Paid:</td>
	<td align="left">' . $invoiceValues['amount_paid'] . '</td>
	</tr>
	<tr>
	<td align="right" colspan="5"><b>Amount Due:</b></td>
	<td align="left">' . $invoiceValues['amount_due'] . '</td>
	</tr>';
$output .= '
	</table>
	</td>
	</tr>
	</table>';
// create pdf of invoice
$invoiceFileName = 'Invoice-' . $invoiceValues['invoiceNo'] . '.pdf';
require_once 'dompdf/src/Autoloader.php';
Dompdf\Autoloader::register();
use Dompdf\Dompdf;
$dompdf = new Dompdf();
$dompdf->loadHtml(html_entity_decode($output));
$dompdf->setPaper('A4', 'landscape');
$dompdf->render();
ob_end_clean();
// $dompdf->stream($invoiceFileName, array("Attachment" => false));
$isUploaded = file_put_contents('purchase_invoices/' . $invoiceFileName, $dompdf->output());

if ($isUploaded != false) {
    header("location:view-purchase-orders.php");
    exit();
}
