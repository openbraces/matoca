<?php
include "application-top.php";

if (!isset($_SESSION["email"])) {
    header("location:index.php");
    exit();
}

$usql = "select * from mtc_usertypes where usertype_id NOT IN ('1')";
$ures = mysqli_query($con, $usql);

if (isset($_GET["mode"])) {
    if ($_GET["mode"] == "publish") {
        if ($_GET["val"] == "pub") {
            $utsql = "update mtc_usertypes set status = 0 where usertype_id=" . $_GET["usertype_id"];
            $uures = mysqli_query($con, $utsql);

            if ($uures) {
                header("location:view-user-types.php");
                exit();
            }
        } else {
            $utsql = "update mtc_usertypes set status = 1 where usertype_id=" . $_GET["usertype_id"];
            $uures = mysqli_query($con, $utsql);

            if ($uures) {
                header("location:view-user-types.php");
                exit();
            }
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php echo $gbl_row["org_name"]; ?></title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/feather/feather.css">
  <link rel="stylesheet" href="vendors/ti-icons/css/themify-icons.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <link rel="stylesheet" href="js/cute-alert-master/alert-style.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/vertical-layout-light/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php include "includes/header.php";?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
    <?php include "includes/right-sidebar.php";?>
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
     <?php include "includes/sidebar-menu.php";?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
        <div class="pl-2 mb-3 float-right">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#utypeModal">Add User Role</button>
        </div>
       <?php create_breadcrumb();?>
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                    <div class="col-md-3 float-right">
                      <input type="text" id="searchText" class="form-control" placeholder="Search...">
                   </div>
                  <h4 class="card-title">Striped Table</h4>
                  <p class="card-description">
                    Add class <code>.table-striped</code>
                  </p>
                  <div class="table-responsive">
                    <table id="usertypeData" class="table table-striped">
                      <thead>
                        <tr>
                          <th>
                            SL No.
                          </th>
                          <th>
                            User Role
                          </th>
                           <th>
                           Added On
                          </th>
                          <th>
                              Previleges
                          </th>
                            <th>
                            Action
                          </th>
                        </tr>
                      </thead>
                      <tbody id="utype">
                        <?php
$i = 0;
if ($ures) {
    while ($urow = mysqli_fetch_array($ures)) {
        $i++;?>
                        <tr>
                          <td>
                            <?php echo $i; ?>.
                          </td>
                          <td>
                            <?php echo ucfirst($urow["usertype_name"]); ?>
                          </td>
                           <td>
                            <?php echo $urow["created_on"]; ?>
                          </td>
                          <td>
                              <a class="btn btn-info btn-rounded btn-icon" style="padding:12px;" href="user-previlege.php?usertype_id=<?php echo $urow["usertype_id"]; ?>"><i class="ti-settings"></i></a>
                          </td>
                           <td>
                             <?php if (obtools::is_usertype_published($urow["usertype_id"])) {?>
                             <a href="view-user-types.php?usertype_id=<?php echo $urow["usertype_id"]; ?>&mode=publish&val=pub" title="Block"><i class="ti-lock icon-sm text-warning"></i></a>
                             <?php } else {?>
                             <a href="view-user-types.php?usertype_id=<?php echo $urow["usertype_id"]; ?>&mode=publish&val=unpub" title="Unlock"><i class="ti-key icon-sm text-info"></i></a>
                             <?php }?>
                             &emsp;
                            <a href="javascript:void(0)" class="edit" data-id="<?php echo $urow["usertype_id"]; ?>" title="Edit" data-toggle="modal"  data-target="#edutypeModal"><i class="ti-user icon-sm text-success"></i></a>
                             &emsp;
                            <a href="javascript:void(0)" class="del" data-id="<?php echo $urow["usertype_id"]; ?>" title="Delete"><i class="ti-trash icon-sm text-danger"></i></a>
                          </td>
                        </tr>
                        <?php
}
}
?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
      <?php include "includes/footer.php";?>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <?php include "includes/common-js.php";?>
  <!-- End custom js for this page-->

 <!-- Start User type modal -->
<div class="modal fade" id="utypeModal" tabindex="-1" role="dialog" aria-labelledby="utypeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="utypeModal">Add New Role</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" id="utFrom">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Enter the user role name</label>
            <input type="text" class="form-control" id="usertype_name" name="usertype_name" required placeholder="User role">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-block btn-primary submit">Create</button>
      </div>
    </div>
  </div>
</div>
<!-- End User type modal -->

 <!-- Start User type modal -->
<div class="modal fade" id="edutypeModal" tabindex="-1" role="dialog" aria-labelledby="edutypeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="edutypeModal">Edit User Role</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" id="edutFrom">
           <span id="res"></span>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-block btn-primary update">Update</button>
      </div>
    </div>
  </div>
</div>
<!-- End User type modal -->

  <script>
   jQuery(document).ready(function($) {

    var utid = "";

    $(".del").click(function(e) {
        utid =  $(this).attr("data-id");

        if(utid !== "" || utid !== undefined || utid !== 0) {
          cuteAlert({
             type: "question",
             title: "Delete Record",
             message: "Do you really want to delete this record?",
             confirmText: "Yes",
             cancelText: "No"
           }).then((e)=>{
           if ( e == ("confirm")) {
               $.ajax({
                   type : 'GET',
                   url : 'ajax/delete-usertype.php?usertype_id='+utid,
                   cache : false,
                   success: function(response) {
                       if(response == 0) {
                         cuteToast({
                           type: "success",
                           message: "User role deleted successfully...",
                           timer: 3000
                        });
                        setTimeout(function() {
                         location.href = "view-user-types.php"
                        }, 3000);
                       } else {
                        cuteToast({
                           type: "error",
                           message: "Something went wrong...",
                           timer: 3000
                        });
                        setTimeout(function() {
                         location.href = "view-user-types.php"
                        }, 3000);
                       }
                   }
               })
          } else {
             // do nothing
          }
       });
        }
    });

    $(".submit").click(function(e){
    e.preventDefault();

    var usertype_name = $("#usertype_name").val();

    if(usertype_name == "") {
      alert ("Please enter the user type name");
    } else {
      $.ajax({
        type: "POST",
        url : "ajax/add-new-user-type.php",
        data: "usertype_name="+usertype_name,
        cache : false,
        success : function(response) {
        if(response == 0) {
          $("#utFrom")[0].reset();
          cuteToast({
            type: "success",
            message: "User role added successfully...",
            timer: 3000
            });
            setTimeout(function() {
            location.href = "view-user-types.php"
            }, 3000);
        } else {
            $("#utFrom")[0].reset();
             cuteToast({
             type: "error",
             message: "Something went wrong...",
             timer: 3000
            });
            setTimeout(function() {
            location.href = "view-user-types.php"
            }, 3000);
            }
        }
      });
    }
   });


   $(".edit").click(function(e) {
        utid =  $(this).attr("data-id");
        if(utid !== "" || utid !== undefined || utid !== 0) {
           $.ajax({
             type : "GET",
             url : "ajax/get-usertype.php?usertype_id="+ utid,
             cache : false,
             success : function(response)
             {
               $("#res").html(response);
             },
             error :  function(err) {
               alert(err);
             }
           })
        }
    });


    $(".update").click(function(e){
    e.preventDefault();

    var eusertype_name = $("#eusertype_name").val();

    if(eusertype_name == "") {
      alert ("Please enter the user type name");
    } else {
      $.ajax({
        type: "POST",
        url : "ajax/update-usertype.php?usertype_id=" + utid,
        data: "usertype_name="+eusertype_name,
        cache : false,
        success : function(response) {
        if(response == 0) {
          cuteToast({
            type: "success",
            message: "User role updated successfully...",
            timer: 3000
            });
            setTimeout(function() {
            location.href = "view-user-types.php"
            }, 3000);
        } else {
             cuteToast({
             type: "error",
             message: "Something went wrong...",
             timer: 3000
            });
            setTimeout(function() {
            location.href = "view-user-types.php"
            }, 3000);
            }
        }
      });
    }
   });

   $("#searchText").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#utype tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
   });

    $('#usertypeData').paging({
     limit:10
    });

   });
  </script>
</body>

</html>
