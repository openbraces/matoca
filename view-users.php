<?php
include "application-top.php";

if (!isset($_SESSION["email"])) {
    header("location:index.php");
    exit();
}

$usql = "select * from mtc_users";
$ures = mysqli_query($con, $usql);

if (isset($_GET["mode"])) {
    if ($_GET["mode"] == "publish") {
        if ($_GET["val"] == "pub") {
            $utsql = "update mtc_users set status = 0 where user_id=" . $_GET["user_id"];
            $uures = mysqli_query($con, $utsql);

            if ($uures) {
                header("location:view-users.php");
                exit();
            }
        } else {
            $utsql = "update mtc_users set status = 1 where user_id=" . $_GET["user_id"];
            $uures = mysqli_query($con, $utsql);

            if ($uures) {
                header("location:view-users.php");
                exit();
            }
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php echo $gbl_row["org_name"]; ?></title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/feather/feather.css">
  <link rel="stylesheet" href="vendors/ti-icons/css/themify-icons.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <link rel="stylesheet" href="js/cute-alert-master/alert-style.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/vertical-layout-light/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php include "includes/header.php";?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
    <?php include "includes/right-sidebar.php";?>
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
     <?php include "includes/sidebar-menu.php";?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <?php create_breadcrumb();?>
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <div class="col-md-3 float-right">
                      <input type="text" id="searchText" class="form-control" placeholder="Search...">
                   </div>
                  <h4 class="card-title">Striped Table</h4>
                  <p class="card-description">
                    Add class <code>.table-striped</code>
                  </p>
                  <div class="table-responsive">
                    <table id="userData" class="table table-striped">
                      <thead>
                        <tr>
                          <th>
                            SL No.
                          </th>
                          <th>
                            User
                          </th>
                          <th>
                            Full name
                          </th>
                          <th>
                            Email ID
                          </th>
                          <th>
                            Last Seen
                          </th>
                            <th>
                            Action
                          </th>
                        </tr>
                      </thead>
                      <tbody id="users">
                        <?php
$i = 0;
if ($ures) {
    while ($urow = mysqli_fetch_array($ures)) {
        $i++;?>
                        <tr>
                          <td>
                            <?php echo $i; ?>.
                          </td>
                          <td class="py-1">
                            <img src="upload_user_image/<?php echo $urow["proflle_image"]; ?>" alt="<?php echo $urow["username"]; ?>"/>
                          </td>
                          <td>
                            <?php echo $urow["username"]; ?>
                          </td>
                          <td>
                            <?php echo $urow["email"]; ?>
                          </td>
                          <td>
                             <?php echo $urow["last_loggedin"]; ?>
                          <!--  May 15, 2015 -->
                          </td>
                           <td>
                             <?php if (obtools::is_user_published($urow["user_id"])) {?>
                             <a href="view-users.php?user_id=<?php echo $urow["user_id"]; ?>&mode=publish&val=pub" title="Block"><i class="ti-lock icon-sm text-warning"></i></a>
                             <?php } else {?>
                             <a href="view-users.php?user_id=<?php echo $urow["user_id"]; ?>&mode=publish&val=unpub" title="Unlock"><i class="ti-key icon-sm text-info"></i></a>
                             <?php }?>
                             &emsp;
                            <a href="edit-user.php?user_id=<?php echo $urow["user_id"]; ?>" title="Edit"><i class="ti-user icon-sm text-success"></i></a>
                             &emsp;
                            <a href="javascript:void(0)" class="del" data-id="<?php echo $urow["user_id"]; ?>" title="Delete"><i class="ti-trash icon-sm text-danger"></i></a>
                          </td>
                        </tr>
                        <?php
}
}
?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
      <?php include "includes/footer.php";?>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <?php include "includes/common-js.php";?>
  <!-- End custom js for this page-->
  <script type="text/javascript">
  jQuery(document).ready(function($){
  $("#searchText").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#users tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
   });

    var uid = "";

    $(".del").click(function(e) {
        uid =  $(this).attr("data-id");

        if(uid !== "" || uid !== undefined || uid !== 0) {
          cuteAlert({
             type: "question",
             title: "Delete Record",
             message: "Do you really want to delete this record?",
             confirmText: "Yes",
             cancelText: "No"
           }).then((e)=>{
           if ( e == ("confirm")) {
               $.ajax({
                   type : 'GET',
                   url : 'ajax/delete-user.php?user_id='+uid,
                   cache : false,
                   success: function(response) {
                       if(response == 0) {
                         cuteToast({
                           type: "success",
                           message: "User deleted successfully...",
                           timer: 3000
                        });
                        setTimeout(function() {
                         location.href = "view-users.php"
                        }, 3000);
                       } else {
                        cuteToast({
                           type: "error",
                           message: "Something went wrong...",
                           timer: 3000
                        });
                        setTimeout(function() {
                         location.href = "view-users.php"
                        }, 3000);
                       }
                   }
               })
          } else {
             // do nothing
          }
       });
        }
    });

    $('#userData').paging({
     limit:20
    });

  });
  </script>
</body>

</html>
