<?php
session_start();

include "includes/connection.php";
include "includes/functions.php";
include "class/utils.php";

/* Authorisation Check */

if (isset($_SESSION["email"])) {
    $pvsql = "select * from mtc_user_previleges where usertype_id =" . $_SESSION["usertype"];
    $pvres = mysqli_query($con, $pvsql);
    $pvrow = mysqli_fetch_array($pvres);

    if ($pvrow["user_module_settings"] !== '1' && stripos($_SERVER['REQUEST_URI'], 'user') !== false) {
        header("location: 403.php");
        exit();
    } else if ($pvrow["product_module_settings"] !== '1' && stripos($_SERVER['REQUEST_URI'], 'product') !== false) {
        header("location: 403.php");
        exit();
    } else if ($pvrow["purchase_module_settings"] !== '1' && stripos($_SERVER['REQUEST_URI'], 'purchase') !== false) {
        header("location: 403.php");
        exit();
    } else if ($pvrow["invoice_module_settings"] !== '1' && stripos($_SERVER['REQUEST_URI'], 'order') !== false) {
        header("location: 403.php");
        exit();
    }
}
/* Authorisation Check */

$gbl_sql = "select * from mtc_global_settings where setting_id = 1";
$gbl_res = mysqli_query($con, $gbl_sql);

if ($gbl_res) {
    $gbl_row = mysqli_fetch_array($gbl_res);
}
