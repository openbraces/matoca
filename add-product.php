<?php
include "application-top.php";

if (!isset($_SESSION["email"])) {
  header("location:index.php");
  exit();
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php echo $gbl_row["org_name"]; ?></title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/feather/feather.css">
  <link rel="stylesheet" href="vendors/ti-icons/css/themify-icons.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <link rel="stylesheet" href="vendors/select2/select2.min.css">
  <link rel="stylesheet" href="vendors/select2-bootstrap-theme/select2-bootstrap.min.css">
  <link rel="stylesheet" href="js/cute-alert-master/alert-style.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/vertical-layout-light/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php include "includes/header.php"; ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
      <?php include "includes/right-sidebar.php"; ?>
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
      <?php include "includes/sidebar-menu.php"; ?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <?php create_breadcrumb(); ?>
          <div class="row">
            <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Add New Product</h4>
                  <form class="form-sample" id="addProduct" method="post" enctype="multipart/form-data">
                    <p class="card-description">
                      Product info
                    </p>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Product Name</label>
                          <div class="col-sm-9">
                            <input type="text" name="product_name" id="product_name" class="form-control" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">SKU Code</label>
                          <div class="col-sm-9">
                            <input type="text" name="product_sku_code" id="product_sku_code" class="form-control" />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Product Price</label>
                          <div class="col-sm-9">
                            <input type="number" name="product_price" id="product_price" class="form-control" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">In Stock</label>
                          <div class="col-sm-9">
                            <input type="number" class="form-control" name="product_instock" id="product_instock" />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group row">
                          <label class="col-sm-1 col-form-label">Detail</label>
                          <div class="col-sm-11">
                            <textarea class="form-control" name="product_desc" id="product_desc" style="margin-left: 4.5%; width: 95.5%;"></textarea>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group row">
                          <label class="col-md-1 col-form-label">Image</label>
                          <input type="file" name="product_image" id="product_image" class="file-upload-default">
                          <div class="input-group col-md-11">
                            <input type="text" class="form-control file-upload-info" style="margin-left: 4.6%;" disabled placeholder="Upload Product Image">
                            <span class="input-group-append">
                              <button class="file-upload-browse btn btn-primary" type="button" style="height: 76%;">Upload</button>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>

                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button class="btn btn-light">Cancel</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <?php include "includes/footer.php"; ?>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <?php include "includes/common-js.php"; ?>
  <script>
    $(document).ready(function() {

      var isValid = false;

      $("#addProduct").on("submit", function(e) {
        e.preventDefault();
        var product_name = $("#product_name").val();
        var product_sku_code = $("#product_sku_code").val();
        var product_price = $("#product_price").val();
        var product_instock = $("#product_instock").val();
        var product_desc = $("#product_desc").val();
        var proData = new FormData();
        proData.append("product_name", product_name);
        proData.append("product_sku_code", product_sku_code);
        proData.append("product_price", product_price);
        proData.append("product_instock", product_instock);
        proData.append("product_desc", product_desc);
        proData.append("product_image", $('input[type=file]')[0].files[0]);

        isvalid = checkFormStatus("addProduct");

        if (isvalid) {
          $.ajax({
            "type": "POST",
            "url": "ajax/add-new-product.php",
            "data": proData,
            dataType: "html",
            contentType: false,
            cache: false,
            processData: false,
            success: function(response) {
              if (response == 0) {
                cuteToast({
                  type: "success",
                  message: "Product added successfully...",
                  timer: 3000
                });
                setTimeout(function() {
                  window.location.href = "view-products.php"
                }, 3000);
              } else if (response == 2) {
                cuteToast({
                  type: "warning",
                  message: "Duplicate SKU is not allowed...",
                  timer: 3000
                });
              } else if (response == 3) {
                cuteToast({
                  type: "warning",
                  message: "Invalid file type, please choose another file.",
                  timer: 3000
                });
              } else {
                cuteToast({
                  type: "error",
                  message: "Something went wrong...",
                  timer: 3000
                });
              }
            }
          })
        }
      });
    });
  </script>
  <!-- End custom js for this page-->
</body>

</html>