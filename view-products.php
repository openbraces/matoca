<?php
include "application-top.php";

if (!isset($_SESSION["email"])) {
    header("location:index.php");
    exit();
}

$psql = "select * from mtc_products";
$pres = mysqli_query($con, $psql);

if (isset($_GET["mode"])) {
    if ($_GET["mode"] == "publish") {
        if ($_GET["val"] == "pub") {
            $prsql = "update mtc_products set status = 0 where product_id=" . $_GET["product_id"];
            $uures = mysqli_query($con, $prsql);

            if ($uures) {
                header("location:view-products.php");
                exit();
            }
        } else {
            $prsql = "update mtc_products set status = 1 where product_id=" . $_GET["product_id"];
            $uures = mysqli_query($con, $prsql);

            if ($uures) {
                header("location:view-products.php");
                exit();
            }
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php echo $gbl_row["org_name"]; ?></title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/feather/feather.css">
  <link rel="stylesheet" href="vendors/ti-icons/css/themify-icons.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <link rel="stylesheet" href="js/cute-alert-master/alert-style.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/vertical-layout-light/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php include "includes/header.php";?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
    <?php include "includes/right-sidebar.php";?>
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
     <?php include "includes/sidebar-menu.php";?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <?php create_breadcrumb();?>
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <div class="col-md-3 float-right">
                      <input type="text" id="searchText" class="form-control" placeholder="Search...">
                   </div>
                  <h4 class="card-title">Striped Table</h4>
                  <p class="card-description">
                    Add class <code>.table-striped</code>
                  </p>
                  <div class="table-responsive">
                    <table id="productData" class="table table-striped">
                      <thead>
                        <tr>
                          <th>
                            SL No.
                          </th>
                          <th>
                          Product Sku Code
                          </th>
                          <th>
                           Porduct Name
                          </th>
                          <th>
                            In Stock
                          </th>
                             <th>
                            Added On
                          </th>
                            <th>
                            Action
                          </th>
                        </tr>
                      </thead>
                      <tbody id="products">
                        <?php
$i = 0;
if ($pres) {
    while ($prow = mysqli_fetch_array($pres)) {
        $i++;?>
                        <tr>
                          <td>
                            <?php echo $i; ?>.
                          </td>
                          <td>
                            <?php echo $prow["product_sku_code"]; ?>
                          </td>
                          <td>
                            <?php echo $prow["product_name"]; ?>
                          </td>
                          <td>
                             <?php echo $prow["product_instock"]; ?>
                          </td>
                          <td> <?php echo $prow["added_on"]; ?></td>
                           <td>
                             <?php if (obtools::is_product_published($prow["product_id"])) {?>
                             <a href="view-products.php?product_id=<?php echo $prow["product_id"]; ?>&mode=publish&val=pub" title="Block"><i class="ti-lock icon-sm text-warning"></i></a>
                             <?php } else {?>
                             <a href="view-products.php?product_id=<?php echo $prow["product_id"]; ?>&mode=publish&val=unpub" title="Unlock"><i class="ti-key icon-sm text-info"></i></a>
                             <?php }?>
                             &emsp;
                            <a href="edit-product.php?product_id=<?php echo $prow["product_id"]; ?>" title="Edit"><i class="ti-user icon-sm text-success"></i></a>
                             &emsp;
                            <a  href="javascript:void(0)" class="del" data-id="<?php echo $prow["product_id"];
        ?>" title="Delete"><i class="ti-trash icon-sm text-danger"></i></a>
                          </td>
                        </tr>
                        <?php
}
}
?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
      <?php include "includes/footer.php";?>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <?php include "includes/common-js.php";?>
  <!-- End custom js for this page-->
  <script type="text/javascript">
  jQuery(document).ready(function($){
  $("#searchText").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#products tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
   });

   var pid = "";

    $(".del").click(function(e) {
        pid =  $(this).attr("data-id");

        if(pid !== "" || pid !== undefined || pid !== 0) {
          cuteAlert({
             type: "question",
             title: "Delete Record",
             message: "Do you really want to delete this record?",
             confirmText: "Yes",
             cancelText: "No"
           }).then((e)=>{
           if ( e == ("confirm")) {
               $.ajax({
                   type : 'GET',
                   url : 'ajax/delete-product.php?product_id='+pid,
                   cache : false,
                   success: function(response) {
                       if(response == 0) {
                         cuteToast({
                           type: "success",
                           message: "Product deleted successfully...",
                           timer: 3000
                        });
                        setTimeout(function() {
                         location.href = "view-products.php"
                        }, 3000);
                       } else {
                        cuteToast({
                           type: "error",
                           message: "Something went wrong...",
                           timer: 3000
                        });
                        setTimeout(function() {
                         location.href = "view-products.php"
                        }, 3000);
                       }
                   }
               })
          } else {
             // do nothing
          }
       });
        }
    });

    $('#productData').paging({
     limit:20
    });

  });
  </script>
</body>

</html>
