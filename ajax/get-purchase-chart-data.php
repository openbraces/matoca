<?php
header('Content-Type: application/json');

include "../application-top.php";

$chsql = "SELECT
    SUM(IF(month = 'Jan', total, 0)) AS 'Jan',
    SUM(IF(month = 'Feb', total, 0)) AS 'Feb',
    SUM(IF(month = 'Mar', total, 0)) AS 'Mar',
    SUM(IF(month = 'Apr', total, 0)) AS 'Apr',
    SUM(IF(month = 'May', total, 0)) AS 'May',
    SUM(IF(month = 'Jun', total, 0)) AS 'Jun',
    SUM(IF(month = 'Jul', total, 0)) AS 'Jul',
    SUM(IF(month = 'Aug', total, 0)) AS 'Aug',
    SUM(IF(month = 'Sep', total, 0)) AS 'Sep',
    SUM(IF(month = 'Oct', total, 0)) AS 'Oct',
    SUM(IF(month = 'Nov', total, 0)) AS 'Nov',
    SUM(IF(month = 'Dec', total, 0)) AS 'Dec'
    FROM (
SELECT DATE_FORMAT(created_on, '%b') AS month, SUM(invoice_total) as total
FROM mtc_orders
WHERE created_on <= NOW() and created_on >= Date_add(Now(),interval - 12 month)
GROUP BY DATE_FORMAT(created_on, '%m-%Y')) as sub";
$chres = mysqli_query($con, $chsql);

$data = array();

while ($row = mysqli_fetch_array($chres)) {
    // $data = $row['Jan'];
    array_push($data, $row['Jan']);
    array_push($data, $row['Feb']);
    array_push($data, $row['Mar']);
    array_push($data, $row['Apr']);
    array_push($data, $row['May']);
    array_push($data, $row['Jun']);
    array_push($data, $row['Jul']);
    array_push($data, $row['Aug']);
    array_push($data, $row['Sep']);
    array_push($data, $row['Oct']);
    array_push($data, $row['Nov']);
    array_push($data, $row['Dec']);
}

echo json_encode($data);
