<?php

include "../application-top.php";

$startDate = $_POST["startDate"] . ' 00:00:00';
$endDate = $_POST["endDate"] . ' 00:00:00';

$sql = "select * from mtc_invoices  where created_on between  '$startDate' and '$endDate'";
$res = mysqli_query($con, $sql);
?>
<thead>
    <tr>
        <th>
            Invoice No.
        </th>
        <th>
            Total Amount
        </th>
        <th>
           Tax Amount
        </th>
        <th>
           Amount Paid
        </th>
        <th>
            Amount Due
        </th>
        <th>
           Products
        </th>
        <th>
            Created On
        </th>
    </tr>
</thead>
<?php
$i = 0;
if ($res) {
    while ($row = mysqli_fetch_array($res)) {
        $i++;
        ?>
<tr>
    <td>
        <?php echo $row["invoiceNo"]; ?>
    </td>
    <td>
        <?php echo $row["invoice_total"]; ?>
    </td>
    <td>
        <?php echo $row["tax"]; ?>
    </td>
    <td>
        <?php echo $row["amount_paid"]; ?>
    </td>
    <td>
        <?php echo $row["amount_due"]; ?>
    </td>
      <td>
         <a class="btn btn-sm btn-primary overview" data-id="<?php echo $row["order_id"]; ?>" href="javascript:void(0)" data-toggle="modal" data-target=".invModal">View</a>
    </td>
    <td>
        <?php echo $row["created_on"]; ?>
    </td>
</tr>
<?php
}
}
?>
