<?php
include "../purchase-invoice.php";

if (isset($_GET["pid"])) {
    $invoiceValues = getInvoice($_GET['pid']);
    $invoiceItems = getInvoiceItems($_GET['pid']);
}
?>
<div class="modal-body">
    <div class="container-fluid">
        <div class="row mb-4">
            <div class="col-md-3">
                Invoice No. : <?php echo $invoiceValues['invoiceNo']; ?>
            </div>
             <div class="col-md-3">
              Customer : <?php echo $invoiceValues['customerName']; ?>
            </div>
            <div class="col-md-3">
                Tax Amount : <?php echo $invoiceValues['tax']; ?>
            </div>
            <div class="col-md-3">
                Total : <?php echo $invoiceValues['invoice_total']; ?>
            </div>
        </div>
        <table class="table  table-striped">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Product SKU Code</th>
                    <th scope="col">Product Name</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Price</th>
                     <th scope="col">Sub Total</th>
                </tr>
            </thead>
            <tbody>
            <?php
$count = 0;
foreach ($invoiceItems as $invoiceItem) {
    $count++;
    ?>
                <tr>
                    <th scope="row"><?php echo $count; ?>.</th>
                    <td><?php echo $invoiceItem["product_sku"]; ?> </td>
                    <td><?php echo $invoiceItem["product_name"]; ?></td>
                    <td><?php echo $invoiceItem["quantity"]; ?></td>
                    <td><?php echo $invoiceItem["price"]; ?></td>
                    <td><?php echo $invoiceItem["quantity"] * $invoiceItem["price"]; ?></td>
                </tr>
                <?php
}
?>
            </tbody>
        </table>
    </div>
</div>
