<?php

include "../application-top.php";

/*if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) { */

$product_name = mysqli_real_escape_string($con, $_POST["product_name"]);
$product_sku_code = mysqli_real_escape_string($con, $_POST["product_sku_code"]);
$product_price = mysqli_real_escape_string($con, $_POST["product_price"]);
$product_instock = mysqli_real_escape_string($con, $_POST["product_instock"]);
$product_desc = mysqli_real_escape_string($con, $_POST["product_desc"]);

if (!empty($_FILES["product_image"]["name"])) {
    $product_image = time() . $_FILES["product_image"]["name"];

    $allowedMimeTypes = array(
        'image/jpg',
        'image/png',
        'image/jpeg',
    );

    $mimeType = mime_content_type($_FILES["product_image"]["tmp_name"]);

    if (!in_array($mimeType, $allowedMimeTypes)) {
        die('3');
    } else {
        move_uploaded_file($_FILES["product_image"]["tmp_name"], "../upload_product_images/" . $product_image);
    }
} else {
    $product_image = "noimage.jpg";
}

/* Check for existing SKU CODE  */

$cksql = "select * from mtc_products where product_sku_code = '$product_sku_code'";
$ckres = mysqli_query($con, $cksql);
$ckcount = mysqli_num_rows($ckres);

if ($ckcount > 0) {
    die("2");
} else {

    $sql = "insert into mtc_products (product_name, product_sku_code, product_price, product_instock, product_desc, product_image, added_on) values ('$product_name', '$product_sku_code', '$product_price', '$product_instock', '$product_desc', '$product_image', NOW())";
    $res = mysqli_query($con, $sql);

    if ($res) {
        echo "0";
    } else {
        echo "1";
    }
}
/*else {
echo "Invalid Request...";
} */
