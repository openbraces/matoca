<?php
header('Content-Type: application/json');

include "../application-top.php";

$prsql = "select SUM(mtc_order_details.quantity) as units, SUM(mtc_order_details.price) as total, mtc_order_details.product_id, mtc_products.product_name FROM `mtc_order_details` inner join mtc_products on mtc_order_details.product_id = mtc_products.product_id group by mtc_order_details.product_id";
$prres = mysqli_query($con, $prsql);

$data = array();

while ($row = mysqli_fetch_array($prres)) {
    $data[] = $row;
}

echo json_encode($data);
