<?php

include "../application-top.php";

/*if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) { */

$gssql = "select * from mtc_global_settings where 	setting_id = '1'";
$gsres = mysqli_query($con, $gssql);

if ($gsres) {
    $gsrow = mysqli_fetch_array($gsres);
}

$org_name = mysqli_real_escape_string($con, $_POST["org_name"]);
$org_info = mysqli_real_escape_string($con, $_POST["org_info"]);
$admin_email = mysqli_real_escape_string($con, $_POST["admin_email"]);
$site_url = mysqli_real_escape_string($con, $_POST["site_url"]);
$contact_no = mysqli_real_escape_string($con, $_POST["contact_no"]);

if (!empty($_FILES["company_logo"]["name"])) {
    $company_logo = time() . $_FILES["company_logo"]["name"];

    $allowedMimeTypes = array(
        'image/svg',
        'image/jpg',
        'image/png',
        'image/jpeg',
    );

    $mimeType = mime_content_type($_FILES["company_logo"]["tmp_name"]);

    if (!in_array($mimeType, $allowedMimeTypes)) {
        die('3');
    } else {
        move_uploaded_file($_FILES["company_logo"]["tmp_name"], "../upload_images/" . $company_logo);
    }
} else {
    $company_logo = $gsrow["company_logo"];
}

$sql = "update mtc_global_settings set  org_name = '$org_name', org_info = '$org_info', admin_email =  '$admin_email', site_url = '$site_url', contact_no = '$contact_no', company_logo = '$company_logo', 	last_updated = NOW()  where setting_id = 1";
$res = mysqli_query($con, $sql);

if ($res) {
    echo "0";
} else {
    echo "1";
}

/*
}
else {
echo "Invalid Request...";
} */
