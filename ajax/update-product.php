<?php

include "../application-top.php";

/*if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) { */

if (isset($_GET["product_id"])) {

    $prsql = "select * from mtc_products where product_id =" . $_GET["product_id"];
    $prres = mysqli_query($con, $prsql);

    if ($prres) {
        $prrow = mysqli_fetch_array($prres);
    }

    $product_name = mysqli_real_escape_string($con, $_POST["product_name"]);
    $product_sku_code = mysqli_real_escape_string($con, $_POST["product_sku_code"]);
    $product_price = mysqli_real_escape_string($con, $_POST["product_price"]);
    $product_instock = mysqli_real_escape_string($con, $_POST["product_instock"]);
    $product_desc = mysqli_real_escape_string($con, $_POST["product_desc"]);

    if (!empty($_FILES["product_image"]["name"])) {
        $product_image = time() . $_FILES["product_image"]["name"];

        $allowedMimeTypes = array(
            'application/msword',
            'application/pdf',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-excel',
            'application/x-pdf',
            'application/vnd.pdf',
            'text/pdf',
            'image/jpg',
            'image/png',
            'image/jpeg',
        );

        $mimeType = mime_content_type($_FILES["product_image"]["tmp_name"]);

        if (!in_array($mimeType, $allowedMimeTypes)) {
            die('3');
        } else {
            move_uploaded_file($_FILES["product_image"]["tmp_name"], "../upload_product_images/" . $product_image);
        }
    } else {
        $product_image = $prrow["product_image"];
    }

    $sql = "update mtc_products set  product_name = '$product_name', product_sku_code = '$product_sku_code', product_price =  '$product_price', product_instock = '$product_instock', product_desc = '$product_desc', product_image = '$product_image'  where product_id =" . $_GET["product_id"];
    $res = mysqli_query($con, $sql);

    if ($res) {
        echo "0";
    } else {
        echo "1";
    }
}
/*
}
else {
echo "Invalid Request...";
} */
