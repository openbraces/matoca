<?php
header('Content-Type: application/json');

include "../application-top.php";

$slsql = "select SUM(mtc_invoice_details.product_id)as units, SUM(mtc_invoice_details.price) as total, mtc_invoice_details.product_id, mtc_products.product_name FROM `mtc_invoice_details` inner join mtc_products on mtc_invoice_details.product_id = mtc_products.product_id group by mtc_invoice_details.product_id";
$slres = mysqli_query($con, $slsql);

$data = array();

while ($row = mysqli_fetch_array($slres)) {
    $data[] = $row;
}

echo json_encode($data);
