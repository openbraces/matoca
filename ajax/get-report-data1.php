<?php
header('Content-Type: application/json');

include "../application-top.php";

$chsql = "SELECT MONTHNAME(created_on) as salesMonth, YEAR(created_on) as year, SUM(invoice_total) as salesAmount FROM mtc_invoices WHERE created_on >= CURDATE() - INTERVAL 12 MONTH GROUP BY LEFT(created_on, 7)";
$chres = mysqli_query($con, $chsql);

$data = array();

while ($row = mysqli_fetch_array($chres)) {
    $data[] = $row;
}

echo json_encode($data);
