<?php
include "../application-top.php";

if (!empty($_POST['type'])) {
    $type = $_POST['type'];
    $name = $_POST['name_startsWith'];
    $sql = "SELECT product_id, product_sku_code, product_name, product_price FROM mtc_products where product_instock !=0 and UPPER($type) LIKE '" . strtoupper($name) . "%'";
    $result = mysqli_query($con, $sql);
    $data = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $name = $row['product_sku_code'] . '|' . $row['product_name'] . '|' . $row['product_price'] . '|' . $row['product_id'];
        array_push($data, $name);
    }
    echo json_encode($data);
    exit;
}
