<?php
ini_set("session.cookie_httponly", 1);

include "application-top.php";
include "includes/encryption.php";



//$userIP = getIP();

if ($_POST["email"] != "" || $_POST["password"] != "") {

   //set login attempt if not set
    if(!isset($_SESSION['attempt'])){
        $_SESSION['attempt'] = 0;
    }

    //check if there are 3 attempts already
    if($_SESSION['attempt'] == 3){
        $_SESSION['error'] = 'Attempt limit reach';
     } else {
    $cipherValue = $_POST['cryptoKey'];
    $Encryption = new Encryption();
    $crypEmail = $Encryption->decrypt($_POST['email'], $cipherValue);
    $crypPass = $Encryption->decrypt($_POST['password'], $cipherValue);

    $email = mysqli_real_escape_string($con, $crypEmail);
    $password = mysqli_real_escape_string($con, sha1(sha1(sha1($crypPass))));

    $sql = "select * from mtc_users where email = '$email' and password = '$password' and status = 1";
    $res = mysqli_query($con, $sql) or die("Query Fail");

    $numrow = mysqli_num_rows($res);

    if ($numrow > 0) {
        $row = mysqli_fetch_array($res);

        $_SESSION["email"] = $email;
        $_SESSION["user_id"] = $row["user_id"];
        $_SESSION["username"] = $row["username"];
        $_SESSION["usertype"] = $row["usertype"];
        $_SESSION["proflle_image"] = $row["proflle_image"];
        //action after a successful login
        //for now just message a successful login
        $_SESSION['success'] = 'Login successful';
        //unset our attempt
        unset($_SESSION['attempt']);

        unset($_SESSION['attempt']);

        unset($_SESSION['attempt']);

        unset($_SESSION['attempt']);

        unset($_SESSION['attempt']);

        unset($_SESSION['attempt']);

        // Create a new CSRF token.
        if (!isset($_SESSION['csrf_token'])) {
            $_SESSION['csrf_token'] = base64_encode(openssl_random_pseudo_bytes(32));
        }

        // if remember me clicked . Values will be stored in $_COOKIE  array

        if (!empty($_POST["rememberMe"]) && $_POST["rememberMe"] == 1) {
            //COOKIES for username

            setcookie("loggedin_user", $_POST["email"], time() + (1 * 365 * 24 * 60 * 60));

            //COOKIES for password

            setcookie("password", $password, time() + (1 * 365 * 24 * 60 * 60));
        } else {
            if (isset($_COOKIE["loggedin_user"])) {
                setcookie("loggedin_user", "");

                if (isset($_COOKIE["password"])) {
                    setcookie("password", "");
                }
            }
        }

        $lg_sql = mysqli_query($con, "update mtc_users set last_loggedin = NOW() where email='" . $_SESSION["email"] . "' and user_id = '" . $row['user_id'] . "'");

        echo "1";
    } else {

       //this is where we put our 3 attempt limit
        $_SESSION['attempt'] += 1;
        //set the time to allow login if third attempt is reach
        if($_SESSION['attempt'] == 3) {
            $_SESSION['reAttempt'] = time() + (5*60);
            //note 5*60 = 5mins, 60*60 = 1hr, to set to 2hrs change it to 2*60*60
        }

        echo "<span> Invalid Email or Password ! </span>";
    }
  }
}
