<?php
class obtools
{
    public static function is_user_published($uid)
    {
        global $con;
        $usql = "select * from mtc_users where user_id='$uid'";
        $ures = mysqli_query($con, $usql);
        $urow = mysqli_fetch_object($ures);

        if ($urow->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function is_product_published($pid)
    {
        global $con;
        $psql = "select * from mtc_products where product_id='$pid'";
        $pres = mysqli_query($con, $psql);
        $prow = mysqli_fetch_object($pres);

        if ($prow->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function is_usertype_published($utid)
    {
        global $con;
        $utsql = "select * from mtc_usertypes where usertype_id='$utid'";
        $utres = mysqli_query($con, $utsql);
        $utrow = mysqli_fetch_object($utres);

        if ($utrow->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function is_image_published($mid)
    {
        global $con;
        $gsql = "select * from fsez_gallery_images where id='$mid'";
        $gres = mysqli_query($con, $gsql);
        $grow = mysqli_fetch_object($gres);

        if ($grow->status == 1) {
            return true;
        } else {
            return false;
        }
    }
    
    public static function is_customer_published($cid)
    {
        global $con;
        $csql = "select * from mtc_customers where customer_id='$cid'";
        $cres = mysqli_query($con, $csql);
        $crow = mysqli_fetch_object($cres);

        if ($crow->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function is_client_published($cid)
    {
        global $con;
        $csql = "select * from mtc_clients where client_id='$cid'";
        $cres = mysqli_query($con, $csql);
        $crow = mysqli_fetch_object($cres);

        if ($crow->status == 1) {
            return true;
        } else {
            return false;
        }
    }


    public static function get_user_name($un)
    {
        global $con;
        $unsql = "select * from mtc_users where user_id='$un'";
        $unres = mysqli_query($con, $unsql);
        $unrow = mysqli_fetch_object($unres);

        echo $unrow->username;

    }

    public static function get_usertype_name($ut)
    {
        global $con;
        $mtsql = "select * from  mtc_usertypes where usertype_id='$ut'";
        $mtres = mysqli_query($con, $mtsql);
        $mtrow = mysqli_fetch_object($mtres);

        echo $mtrow->usertype_name;

    }

    public static function get_last_loggedin_time($id)
    {
        global $con;
        $lsql = "select * from  mtc_users where user_id='$id'";
        $lres = mysqli_query($con, $lsql);
        $lrow = mysqli_fetch_object($lres);

        echo $lrow->last_loggedin;

    }

    public static function get_total_users()
    {
        global $con;
        $usql = "select count(*) as total_user from mtc_users where status =1";
        $ures = mysqli_query($con, $usql);
        $urow = mysqli_fetch_object($ures);

        echo $urow->total_user;

    }

}
