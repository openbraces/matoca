<?php
include "application-top.php";

$insql = "select * from mtc_orders order by created_on desc";
$inres = mysqli_query($con, $insql);
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php echo $gbl_row["org_name"]; ?></title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/feather/feather.css">
  <link rel="stylesheet" href="vendors/ti-icons/css/themify-icons.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/vertical-layout-light/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php include "includes/header.php";?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
      <?php include "includes/right-sidebar.php";?>
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
     <?php include "includes/sidebar-menu.php";?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
           <?php create_breadcrumb();?>
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                    <div class="col-md-3 float-right">
                      <input type="text" id="searchText" class="form-control" placeholder="Search...">
                   </div>
                  <h4 class="card-title">Striped Table</h4>
                  <p class="card-description">
                    Add class <code>.table-striped</code>
                  </p>
                  <div class="table-responsive">
                    <table id="purchaseData" class="table table-striped">
                      <thead>
                        <tr>
                          <th>
                            SL No.
                          </th>
                          <th>
                            Invoice No.
                          </th>
                          <th>
                            Customer Info
                          </th>
                          <th>
                         Created On
                          </th>
                          <th>
                         Overview
                          </th>
                            <th>
                        Action
                          </th>
                        </tr>
                      </thead>
                      <tbody id="porders">
                        <?php
$i = 0;
if ($inres) {
    while ($inrow = mysqli_fetch_array($inres)) {
        $i++;
        ?>
                        <tr>
                          <td>
                            <?php echo $i; ?>.
                          </td>
                          <td>
                            <?php echo $inrow["invoiceNo"]; ?>
                          </td>
                          <td>
                            <?php echo $inrow["customerName"]; ?>
                          </td>
                          <td>
                             <?php echo $inrow["created_on"]; ?>
                          </td>
                          <td>
                              <a class="btn btn-sm btn-primary overview" data-id="<?php echo $inrow["purchase_order_id"]; ?>" href="javascript:void(0)" data-toggle="modal" data-target=".invModal">View</a>
                          </td>
                           <td>
                             <a  target="_blank" href="purchase_invoices/Invoice-<?php echo $inrow["invoiceNo"]; ?>.pdf" class="btn btn-info btn-sm btn-icon-text">
                          Print
                          <i class="ti-printer btn-icon-append"></i>
                        </a>
                          </td>
                        </tr>
                        <?php
}
}
?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
      <?php include "includes/footer.php";?>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <?php include "includes/common-js.php";?>
  <!-- End custom js for this page-->

  <!-- Invoice Modal -->
  <div class="modal fade invModal" tabindex="-1" role="dialog" aria-labelledby="invModalModal" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="invModalTitle">Purchase Order Summary</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
      <div id="pdata"></div>
      </div>
   </div>
</div>
 <!-- Invoice Modal -->

 <script>
jQuery(document).ready(function($){

  var pid = "";
		 $(".overview").click(function(){
		   pid = $(this).attr("data-id");
			if (pid)
			{
			$.ajax({
				type: "GET",
				url:  "ajax/get-purchase-data.php?pid="+pid,
				cache: false,
				success: function(response) {
				 $("#pdata").html(response);
				}
			});
			}
		 });

  $("#searchText").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#porders tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
   });

    $('#purchaseData').paging({
     limit:20
    });

});
</script>
</body>

</html>
