<?php
include "includes/connection.php";

function saveInvoice(array $data)
{

    if (!empty($data)) {
        global $con;

        $invoice_data = array();

        /*  $count = 0;
        if (isset($data['data'])) {
        foreach ($data['data'] as $value) {
        var_dump($value);
        if (!empty($value['product_id'])) {
        $count++;
        }
        }
        }

        if ($count == 0) {
        throw new Exception("Please add atleast one product to invoice.");
        } */

        // escape variables for security
        if (!empty($data)) {
            $client_id = mysqli_real_escape_string($con, trim($data['client_id']));
            $invoice_total = mysqli_real_escape_string($con, trim($data['invoice_total']));
            $invoice_subtotal = mysqli_real_escape_string($con, trim($data['invoice_subtotal']));
            $tax = mysqli_real_escape_string($con, trim($data['tax']));
            $tax_percent = mysqli_real_escape_string($con, trim($data['tax_percent']));
            $amount_paid = mysqli_real_escape_string($con, trim($data['amount_paid']));
            $amount_due = mysqli_real_escape_string($con, trim($data['amount_due']));
            $invoiceNo = mysqli_real_escape_string($con, trim($data['invoiceNo']));
            $customerName = mysqli_real_escape_string($con, trim($data['customerName']));
            $customerContact = mysqli_real_escape_string($con, trim($data['customerContact']));
            $customerAddress = mysqli_real_escape_string($con, trim($data['customerAddress']));

            $id = mysqli_real_escape_string($con, trim($data['id']));

            if (empty($id)) {
                $uuid = uniqid();
                $query = "insert into mtc_orders (`client_id`,  `invoice_total`, `invoice_subtotal`, `tax`, `tax_percent`,
							`amount_paid`, `amount_due`, `customerName`, `customerContact`, `customerAddress`, `invoiceNo`, `created_on`, `uuid`)
							values ('$client_id',  '$invoice_total', '$invoice_subtotal', '$tax', '$tax_percent', '$amount_paid', '$amount_due', '$customerName', '$customerContact', '$customerAddress', '$invoiceNo',
							CURRENT_TIMESTAMP, '$uuid')";

            } else {
                $uuid = $data['uuid'];
                $query = "update `mtc_orders` set `client_id` = '$client_id', `invoice_total` ='$invoice_total',`invoice_subtotal` = '$invoice_subtotal',
							`tax` = '$tax', `amount_paid` = '$amount_paid', `tax_percent` = '$tax_percent', `invoiceNo` = '$invoiceNo', `amount_due` = '$amount_due', `customerName` = '$customerName', `customerContact` = '$customerContact', `customerAddress` = '$customerAddress', `updated` = CURRENT_TIMESTAMP
							where `id` = $id";
            }
            if (!mysqli_query($con, $query)) {
                throw new Exception(mysqli_error($con));
            } else {
                if (empty($id)) {
                    $id = mysqli_insert_id($con);
                }

                foreach ($data["product_sku_code"] as $row => $product_sku_code) {
                    $product_sku_code = mysqli_real_escape_string($con, $data['product_sku_code'][$row]);
                    $product_id = mysqli_real_escape_string($con, $data['product_id'][$row]);
                    $product_name = mysqli_real_escape_string($con, $data['product_name'][$row]);
                    $quantity = mysqli_real_escape_string($con, $data['quantity'][$row]);
                    $price = mysqli_real_escape_string($con, $data['price'][$row]);
                    $invoice_id = $id;

                    $invoice_data[] = "('$invoice_id','$product_id','$product_sku_code','$product_name','$quantity','$price')";

                }

                $query = "INSERT INTO mtc_order_details (`purchase_invoice_id`, `product_id`, product_sku, product_name, `quantity`, `price`)
                VALUES " . implode(',', $invoice_data);
                $res = mysqli_query($con, $query);

                if ($res) {

                    foreach ($data["product_id"] as $prd => $product_id) {

                        $product_id = mysqli_real_escape_string($con, $data['product_id'][$prd]);
                        $quantity = mysqli_real_escape_string($con, $data['quantity'][$prd]);

                        $psql = "select product_id, product_instock from  mtc_products where product_id =" . $product_id;
                        $pres = mysqli_query($con, $psql);

                        while ($prow = mysqli_fetch_array($pres)) {
                            $pdsql = "update mtc_products set product_instock = '" . $prow["product_instock"] . "' + $quantity  where product_id=" . $product_id;
                            $pdres = mysqli_query($con, $pdsql);
                        }
                    }

                }

            }

            return [
                'success' => true,
                'uuid' => $uuid,
                'message' => 'Invoice Saved Successfully.',
                'purchase_order_id' => $id,
            ];
        } else {
            throw new Exception("Please check, some of the required fileds missing");
        }
    } else {
        throw new Exception("Please check, some of the required fileds missing");
    }

}

function getInvoices()
{
    global $con;
    $data = [];
    $query = "SELECT * FROM mtc_orders";
    if ($result = mysqli_query($con, $query)) {
        while ($row = mysqli_fetch_assoc($result)) {
            array_push($data, $row);
        }
    }
    return $data;
}

function getInvoice($invoiceId)
{
    global $con;
    $data = [];
    $query = "SELECT * FROM mtc_order_details inner join mtc_orders
	        on mtc_order_details.purchase_invoice_id = mtc_orders.purchase_order_id
			WHERE purchase_invoice_id = '$invoiceId'";

    $result = mysqli_query($con, $query);
    $row = mysqli_fetch_array($result);
    return $row;

}

function getInvoiceItems($invoiceId)
{
    global $con;

    $sqlQuery = "SELECT * FROM mtc_order_details
			WHERE purchase_invoice_id = '$invoiceId'";
    $result = mysqli_query($con, $sqlQuery);
    if (!$result) {
        die('Error in query: ' . mysqli_error($con));
    }
    $data = array();
    while ($row = mysqli_fetch_array($result)) {
        $data[] = $row;
    }
    return $data;
}
