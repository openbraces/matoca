<?php
require_once 'config.php';
require_once 'invoice-save.php';

$invoices = getInvoices();
?>
<!DOCTYPE html>
<html>
	<head>
        <meta charset="utf-8">  
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
                    
        <meta name="author" content="muni">
        <meta name="description" content="Save Multiple Rows of Invoice Data In MySQL Database Using PHP, jQuery and Bootstrap 3">
   	 	<meta name="keywords" content="jquery autocomplete invoice, jquery autocomplete invoice module, invoice using jqueryautocomplete, jquery invoice module  autocomplete, invoice using jquery autocomplete">
    	
        
        
        <title>Save Multiple Rows of Invoice Data In MySQL Database Using PHP, jQuery and Bootstrap 3</title>
        <link href="css/jquery-ui.min.css" rel="stylesheet">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/datepicker.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">
    </head>

    <body>  
    	<?php include_once './templates/nav.php'; ?>
        <!-- Insert your HTML here -->
        <div class="container content">
        	<?php include_once './templates/message.php'; ?>
        	<h2>Invoices</h2>
        	<div class="table-responsive">
			  <table class="table table-condensed">
				 <thead>
					<tr>
						<th>#</th>
						<th>Client Name</th>
						<th>Invoice Total</th>
						<th>Amount Paid</th>
						<th>Due Amount</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						if( !empty( $invoices ) ){ 
							foreach ($invoices as $value) {
					?>
					<tr>
						<th scope="row"><?= isset($value['id']) ? $value['id'] : '-' ?> </th>
						<td>Smart Invoice</td>
						<td> <?= isset($value['invoice_total']) ? $value['invoice_total'] : '-' ?> </td>
						<td> <?= isset($value['amount_paid']) ? $value['amount_paid'] : '-' ?> </td>
						<td> <?= isset($value['amount_due']) ? $value['amount_due'] : '-' ?> </td>
					</tr>
					<?php } } else { ?>
					<tr colspan="4">
						<td>No Invoice Found</td>
					</tr>
					<?php } ?>
				</tbody>
			  </table>
			</div>
        </div> <!-- /container -->
        
		<?php include_once './templates/footer.php'; ?>
        <script src="js/jquery.min.js"></script>
		<script src="js/jquery-ui.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/bootstrap-datepicker.js"></script>
		<script src="js/auto.js"></script>
        
    </body>
</html>