<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12">
			<h2 class="text-center">
				<a href="#">Back To Tutorial</a>
			</h2>
		</div>
	</div>
</div>
<h2>&nbsp;</h2>
<footer class="footer">
  <div class="container">
	<p class="text-muted text-center" style="color:#fff"> &copy; Copyright By <a href="http://smarttutorials.net/" target="_blank" style="color:#bdc3c7">SmartTutorials.net</a>.</p>
  </div>
</footer>