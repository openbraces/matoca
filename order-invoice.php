<?php
include "includes/connection.php";

function saveInvoice(array $data)
{
    if (!empty($data)) {
        global $con;

        $invoice_data = array();

        // escape variables for security
        if (!empty($data)) {
            $client_id = mysqli_real_escape_string($con, trim($data['client_id']));
            $invoice_total = mysqli_real_escape_string($con, trim($data['invoice_total']));
            $invoice_subtotal = mysqli_real_escape_string($con, trim($data['invoice_subtotal']));
            $tax = mysqli_real_escape_string($con, trim($data['tax']));
            $tax_percent = mysqli_real_escape_string($con, trim($data['tax_percent']));
            $amount_paid = mysqli_real_escape_string($con, trim($data['amount_paid']));
            $amount_due = mysqli_real_escape_string($con, trim($data['amount_due']));
            $invoiceNo = mysqli_real_escape_string($con, trim($data['invoiceNo']));
            $customerName = mysqli_real_escape_string($con, trim($data['customerName']));
            $customerContact = mysqli_real_escape_string($con, trim($data['customerContact']));
            $customerAddress = mysqli_real_escape_string($con, trim($data['customerAddress']));

            $id = mysqli_real_escape_string($con, trim($data['id']));

            if (empty($id)) {
                $uuid = uniqid();
                $query = "insert into mtc_invoices (`client_id`,  `invoice_total`, `invoice_subtotal`, `tax`, `tax_percent`,
							`amount_paid`, `amount_due`, `customerName`, `customerContact`, `customerAddress`, `invoiceNo`, `created_on`, `uuid`)
							values ('$client_id',  '$invoice_total', '$invoice_subtotal', '$tax', '$tax_percent', '$amount_paid', '$amount_due', '$customerName', '$customerContact', '$customerAddress', '$invoiceNo',
							CURRENT_TIMESTAMP, '$uuid')";

            } else {
                $uuid = $data['uuid'];
                $query = "UPDATE `mtc_invoices` SET `client_id` = '$client_id', `invoice_total` ='$invoice_total',`invoice_subtotal` = '$invoice_subtotal',
							`tax` = '$tax', `amount_paid` = '$amount_paid', `amount_due` = '$amount_due', `customerName` = '$customerName', `customerContact` = '$customerContact', `customerAddress` = '$customerAddress', `updated` = CURRENT_TIMESTAMP
							WHERE `id` = $id";
            }
            if (!mysqli_query($con, $query)) {
                throw new Exception(mysqli_error($con));
            } else {
                if (empty($id)) {
                    $id = mysqli_insert_id($con);
                }

                foreach ($data["product_sku_code"] as $row => $product_sku_code) {
                    $product_sku_code = mysqli_real_escape_string($con, $data['product_sku_code'][$row]);
                    $product_id = mysqli_real_escape_string($con, $data['product_id'][$row]);
                    $product_name = mysqli_real_escape_string($con, $data['product_name'][$row]);
                    $quantity = mysqli_real_escape_string($con, $data['quantity'][$row]);
                    $price = mysqli_real_escape_string($con, $data['price'][$row]);
                    $invoice_id = $id;

                    $invoice_data[] = "('$invoice_id','$product_id','$product_sku_code','$product_name','$quantity','$price')";

                }

                $query = "INSERT INTO mtc_invoice_details (`invoice_id`, `product_id`, product_sku, product_name, `quantity`, `price`)
                VALUES " . implode(',', $invoice_data);
                $res = mysqli_query($con, $query);

                if ($res) {

                    foreach ($data["product_id"] as $prd => $product_id) {

                        $product_id = mysqli_real_escape_string($con, $data['product_id'][$prd]);
                        $quantity = mysqli_real_escape_string($con, $data['quantity'][$prd]);

                        $psql = "select product_id, product_instock from  mtc_products where product_id =" . $product_id;
                        $pres = mysqli_query($con, $psql);

                        while ($prow = mysqli_fetch_array($pres)) {
                            $pdsql = "update mtc_products set product_instock = '" . $prow["product_instock"] . "' - $quantity  where product_id=" . $product_id;
                            $pdres = mysqli_query($con, $pdsql);
                        }
                    }

                }

            }
            return [
                'success' => true,
                'uuid' => $uuid,
                'message' => 'Invoice Saved Successfully.',
                'order_id' => $id,
            ];
        } else {
            throw new Exception("Please check, some of the required fileds missing");
        }
    } else {
        throw new Exception("Please check, some of the required fileds missing");
    }
}

function getInvoice($invoiceId)
{
    global $con;
    $query = "SELECT * FROM mtc_invoice_details inner join mtc_invoices on mtc_invoice_details.invoice_id = mtc_invoices.order_id
			           WHERE invoice_id = '$invoiceId'";
    $result = mysqli_query($con, $query);
    $row = mysqli_fetch_array($result);
    return $row;
}

function getInvoiceItems($invoiceId)
{
    global $con;

    $sqlQuery = "SELECT * FROM mtc_invoice_details WHERE invoice_id = '$invoiceId'";
    $result = mysqli_query($con, $sqlQuery);
    if (!$result) {
        die('Error in query: ' . mysqli_error($con));
    }
    $data = array();
    while ($row = mysqli_fetch_array($result)) {
        $data[] = $row;
    }
    return $data;
}
