<?php
include "application-top.php";

if (!isset($_SESSION["email"])) {
    header("location:index.php");
    exit();
}

if (isset($_GET["usertype_id"])) {

    $sql = "select * from mtc_user_previleges where usertype_id =" . $_GET["usertype_id"];
    $res = mysqli_query($con, $sql);
    if ($res) {
        $row = mysqli_fetch_array($res);
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php echo $gbl_row["org_name"]; ?></title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/feather/feather.css">
  <link rel="stylesheet" href="vendors/ti-icons/css/themify-icons.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <link rel="stylesheet" href="vendors/select2/select2.min.css">
  <link rel="stylesheet" href="vendors/select2-bootstrap-theme/select2-bootstrap.min.css">
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="js/cute-alert-master/alert-style.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/vertical-layout-light/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
   <?php include "includes/header.php";?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
      <?php include "includes/right-sidebar.php";?>
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
       <?php include "includes/sidebar-menu.php";?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
           <?php create_breadcrumb();?>
          <div class="row">
            <div class="col-12 grid-margin">
             <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Previleges of <?php if (obtools::get_usertype_name($row["usertype_id"]));?></h4>
                  <p class="card-description">Checkbox and radio controls (default appearance is in primary color)</p>
                  <form id="uprevForm">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="form-check">
                            <label class="form-label">
                              User Management
                            </label>
                          </div>
                          <div class="form-check">
                            <label class="form-label">
                               Product Management
                            </label>
                          </div>
                          <div class="form-check">
                            <label class="form-label">
                               Order Management
                           </label>
                          </div>
                          <div class="form-check">
                            <label class="form-label">
                              Purchase Management
                           </label>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="form-check">
                           <div class="custom-control custom-switch">
                              <input type="checkbox" class="custom-control-input" <?php if ($row['user_module_settings'] == 1) {echo "checked='checked'";}?>
                               name="user_module_settings"  id="user_module_settings">
                              <label class="custom-control-label" for="user_module_settings" ></label>
                           </div>
                        </div>
                          <div class="form-check">
                          <div class="custom-control custom-switch">
                              <input type="checkbox" class="custom-control-input" <?php if ($row['product_module_settings'] == 1) {echo "checked='checked'";}?>
 name="product_module_settings"   id="product_module_settings">
                              <label class="custom-control-label" for="product_module_settings"></label>
                           </div>
                          </div>
                          <div class="form-check">
                            <div class="custom-control custom-switch">
                              <input type="checkbox" class="custom-control-input" <?php if ($row['invoice_module_settings'] == 1) {echo "checked='checked'";}?>
 name="invoice_module_settings"   id="invoice_module_settings">
                              <label class="custom-control-label" for="invoice_module_settings"></label>
                           </div>
                          </div>
                          <div class="form-check">
                            <div class="custom-control custom-switch">
                              <input type="checkbox" class="custom-control-input" <?php if ($row['purchase_module_settings'] == 1) {echo "checked='checked'";}?>
 name="purchase_module_settings"   id="purchase_module_settings">
                              <label class="custom-control-label" for="purchase_module_settings"></label>
                           </div>
                          </div>
                        </div>
                      </div>
                    </div>
                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <?php include "includes/footer.php";?>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <?php include "includes/common-js.php";?>

  <script>

   $(document).ready(function() {
       $("#uprevForm").on("submit", function(e) {
           e.preventDefault();
           var user_module_settings = $("#user_module_settings").prop("checked") ? 1 : 0;
           var product_module_settings = $("#product_module_settings").prop("checked") ? 1 : 0;
           var invoice_module_settings = $("#invoice_module_settings").prop("checked") ? 1 : 0;
           var purchase_module_settings = $("#purchase_module_settings").prop("checked") ? 1 : 0;

           var dataStr = "user_module_settings="+user_module_settings+"&product_module_settings="+product_module_settings+"&invoice_module_settings="+invoice_module_settings+"&purchase_module_settings="+purchase_module_settings;
           var usertype_id = <?php echo $_GET["usertype_id"]; ?>

           $.ajax({
                type : "POST",
                url    : "ajax/update-user-previlege.php?usertype_id="+ usertype_id,
                data : dataStr,
                cache: false,
               success: function(response) {
                if(response == 0)
                  {
                 cuteToast({
                 type: "success",
                 message: "Previleges updated successfully...",
                 timer: 3000
                });
              setTimeout(function() {
               window.location.href="view-user-types.php"} , 3000);
			         } else {
                cuteToast({
                 type: "error",
                 message: "Something went wrong...",
                 timer: 3000
                 });
                  }
               }
           })
       });
   });
  </script>
  <!-- End custom js for this page-->
</body>

</html>
