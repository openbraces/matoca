function checkFormStatus(formId) {
    var status = true;

    if (status == true) {

        isFormValid(formId);

        if ($("#" + formId).valid()) {
            status = true;
        } else
            status = false;
    }

    if (!status) {
        pageScrollTop();
    }
    return status;
}



function isFormValid(formId) {    

   $("#" + formId).validate({
    rules: {
        product_name: {
            required: true
        },
        product_sku_code : {
            required : true,
        },
        product_price: {
            required : true,
            number: true
        },
        proflle_image: {
            required: true
        },
        username: {
            required: true,
            minlength: 2
        },
        password: {
            required: true,
            minlength: 5
        },
        confirm_password: {
            required: true,
            minlength: 5,
            equalTo: "#password"
        },
        email: {
            required: true,
            email: true
        },
        topic: {
            required: "#newsletter:checked",
            minlength: 2
        },
        usertype: "required"
    },
    messages: {
        product_name: {
            required : "Please enter the product name."
        },
        product_sku_code: {
            required : "Please enter the product sku code."
        },
        username: {
            required: "Please enter a username.",
            minlength: "Your username must consist of at least 3 characters."
        },
        password: {
            required: "Please provide a password.",
            minlength: "Your password must be at least 5 characters long."
        },
        confirm_password: {
            required: "Please provide a password",
            minlength: "Your password must be at least 5 characters long",
            equalTo: "Please enter the same password as above"
        },
        email: {
            required: "Please enter a email address.",
            email: "Please enter a valid email address.",
        },
        product_price : {
            required: "Please enter the product price.",
            number: "Please enter a valid numeric value.",
        },
        usertype: "Please select an user role.",
        topic: "Please select at least 2 topics",
        proflle_image: "Please upload an image."
    }
});

}


function pageScrollTop() {
    jQuery("html, body").animate({
        scrollTop: 0,
    });
}
