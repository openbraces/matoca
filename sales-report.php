<?php
include "application-top.php";

if (!isset($_SESSION["email"])) {
    header("location:index.php");
    exit();
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php echo $gbl_row["org_name"]; ?></title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/feather/feather.css">
  <link rel="stylesheet" href="vendors/ti-icons/css/themify-icons.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <link href="css/datepicker.css" rel="stylesheet">

    <link rel="stylesheet" href="js/cute-alert-master/alert-style.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/vertical-layout-light/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php include "includes/header.php";?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
       <?php include "includes/right-sidebar.php";?>
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
     <?php include "includes/sidebar-menu.php";?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
         <?php create_breadcrumb();?>
         <div class="row">
            <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Select  Date Range</h4>
                  <p class="card-description">
                    Use the <code>.form-inline</code> class to display a series of labels, form controls, and buttons on
                    a single horizontal row
                  </p>
                  <form class="form-inline" id="salesForm" method="post" action="">
                    <label class="sr-only" for="startDate">Start Date</label>
                    <input type="text" readonly autocomplete="off" name="startDate" id="startDate" class="form-control col-5 mb-2 mr-sm-2"  placeholder="Start Date">

                    <label class="sr-only" for="endDate">End Date</label>
                    <input type="text" readonly autocomplete="off" name="endDate" id="endDate" class="form-control col-5 mb-2 mr-sm-2"  placeholder="End Date">

                    <button type="submit" class="btn btn-info mb-2">Submit</button>
                  </form>
                </div>
              </div>
            </div>
         </div>

          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Striped Table</h4>
                  <p class="card-description">
                    Add class <code>.table-striped</code>
                  </p>
                  <div class="table-responsive">
                    <table class="table table-striped" id="report">
                      <tbody>
                         <div class="text-center text-primary nodata">No records found...</span>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
      <?php include "includes/footer.php";?>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <?php include "includes/common-js.php";?>
  <!-- End custom js for this page-->

    <!-- Invoice Modal -->
  <div class="modal fade invModal" tabindex="-1" role="dialog" aria-labelledby="invModal" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="invModalTitle">Order Detail</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
      <div id="indata"></div>
      </div>
   </div>
</div>
 <!-- Invoice Modal -->

  <script>
      //datepicker
    $(function () {
	  $('#startDate, #endDate').datepicker({
                showButtonPanel: true,
                changeMonth: true,
                format: 'yyyy-mm-dd',
                autoclose: true
    });

   $(".nodata").show();

   $("#salesForm").on('submit', function(e) {
     e.preventDefault();

   var startDate = $("#startDate").val();
   var endDate  = $("#endDate").val();

   var dateRange = "startDate="+startDate+"&endDate="+endDate;

   if(startDate == "" || endDate == "") {
    cuteAlert({
     type: "error",
     title: "Invalid Dates",
     message: "Please select valid dates",
     buttonText: "Okay"
     });
   } else if  ((Date.parse(endDate) <= Date.parse(startDate))) {
     cuteAlert({
     type: "warning",
     title: "Invalid Date Range",
     message: "End date should be greater than Start date",
     buttonText: "Okay"
     });
      document.getElementById("endDate").value = "";
   } else {
     $.ajax({
       type : "POST",
       url : "ajax/get-sales-data.php",
       data : dateRange,
       cache : false,
       success : function(response) {
         $("#report").html(response);
         $(".nodata").hide();
       }
     })
   }
   });

   	$(document).on("click", "#report tbody tr .overview", function(){
		   pid = $(this).attr("data-id");
			if (pid)
			{
			$.ajax({
				type: "GET",
				url:  "ajax/get-invoice-data.php?pid="+pid,
				cache: false,
				success: function(response) {
				 $("#indata").html(response);
				}
			});
			}
		 });
});
</script>
</body>

</html>
